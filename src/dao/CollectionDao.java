package dao;

import java.sql.SQLException;
import java.util.List;

import vo.Collection;

public interface CollectionDao extends Dao {
	/**
	 * 根据用户id查找其所有收藏
	 * @param uid 用户id
	 * @return
	 */
	public List<Collection> findCollections(int uid) throws SQLException;
	
	public Collection findCollection(int uid, int fid) throws SQLException;
}
