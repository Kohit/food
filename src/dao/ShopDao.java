package dao;

import java.sql.SQLException;
import java.util.List;

import vo.Shop;

public interface ShopDao extends Dao {

	/**
	 * 根据店铺名进行模糊查找
	 * @param name 店铺名
	 * @return
	 */
	public List<Shop> searchShop(String name) throws SQLException;
	
	/**
	 * 查找指定店铺
	 * @param name
	 * @return
	 */
	public Shop findShop(String name) throws SQLException;
	
	/**
	 * 查找指定店铺名的店铺
	 * @param name
	 * @return
	 */
	public Shop findShopByShopName(String name) throws SQLException;
	
	public List<Shop> getTopShops() throws SQLException;
}
