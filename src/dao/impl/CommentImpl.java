package dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.Comment;
import dao.CommentDao;

public class CommentImpl extends DaoImpl implements CommentDao {

	@Override
	public List<Comment> findCommentsByFood(int fid) throws SQLException {
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Comment where food.id = :fid");
		   query.setLong("fid",fid);
		   List<Comment> list = new ArrayList<Comment>();
		   list = query.list();
		   session.close();    
		return list;
	
	}

	@Override
	public List<Comment> findCommentsByUser(int uid) throws SQLException{
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Comment where user.id =:uid");
		   query.setLong("uid",uid);
		   List<Comment> list = new ArrayList<Comment>();   
		   list= query.list();
		   session.close();
		return list;
	}

	@Override
	public Comment findComment(int uid, int fid) throws SQLException {
		Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Comment where user.id =:uid and food.id =:fid");
		   query.setLong("uid",uid);
		   query.setLong("fid", fid);
		   Comment co = (Comment)query.uniqueResult(); 
		   session.close();
		   return co;
	}

}
