package dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import tools.ResultSet;
import dao.Dao;

public class DaoImpl implements Dao {
	
	protected SessionFactory sessionFactory;
	
	public DaoImpl(){
		Configuration cfg = new Configuration();
		sessionFactory = cfg.configure().buildSessionFactory();
	}
	
	@Override
	public int add(Object entity) throws SQLException{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int id = (int)session.save(entity);
		tx.commit();
		session.close();
		return id;
	}

	@Override
	public <T> void delete(Class<T> entityClass, Object entityid) throws SQLException{

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();	
		String name = entityClass.getName();
		int id = (int) entityid;
		session.createQuery("delete "+name+" where id = "+id+"").executeUpdate();	
		tx.commit();
		session.close();
	}

	@Override
	public void update(Object entity) throws SQLException{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(entity);
		tx.commit();
		session.close();
	}

	@Override
	public <T> T find(Class<T> entityClass, Object entityid) throws SQLException{
		Session session = sessionFactory.openSession();
	    String name = entityClass.getName();
	    int id = (int) entityid;
		T object = (T) session.get(name, id);	
		session.close();
		return object;
	}

	@Override
	public <T> ResultSet<T> select(Class<T> entityClass, int firstIndex,
			int length, String orderby) throws SQLException{
		
		Session session = sessionFactory.openSession();
		String classname = entityClass.getName();
		Query query = session.createQuery("from "+classname+" "+orderby+"");
		query.setFirstResult(firstIndex);
		query.setMaxResults(length);
		ResultSet<T> res = new ResultSet<T>();
		res.setList(query.list());
		res.setTotalRecord((Long)session.createQuery("select count(*) from "+classname+"").uniqueResult());
		session.close();
		return res;
	}

	@Override
	public <T> ResultSet<T> select(Class<T> entityClass, int firstIndex,
			int length) throws SQLException{
		
		Session session = sessionFactory.openSession();
		String classname = entityClass.getClass().getName();
		Query query = session.createQuery("from "+classname+"");
		query.setFirstResult(firstIndex);
		query.setMaxResults(length);
		ResultSet<T> res = new ResultSet<T>();
		res.setList(query.list());
		res.setTotalRecord((Long)session.createQuery("select count(*) from "+classname+"").uniqueResult());
		session.close();
		return res;
	}

	@Override
	public <T> List<T> select(Class<T> entityClass, String option) throws SQLException{
		Session session = sessionFactory.openSession();
		String classname = entityClass.getName();
		Query query = session.createQuery("from "+classname+" "+option+"");
		List<T> list = query.list();
		session.close();
		return list;
	}

	
}
