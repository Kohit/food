package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import vo.Food;
import vo.Shop;
import dao.Factory;
import dao.ShopDao;
public class ShopImpl extends DaoImpl implements ShopDao {

	@Override
	public List<Shop> searchShop(String name) throws SQLException{
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Shop where name like :sname");
		   query.setString("sname","%"+name+"%");
		   List<Shop> list = new ArrayList<Shop>();
	       list = query.list();
		   session.close();  
		   
		  return  list;
	}

	@Override
	public Shop findShop(String name) throws SQLException{
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Shop where name =:sname");
		   query.setString("sname",name);
		   Shop shop = (Shop)query.uniqueResult();
		   session.close();
          return shop;
	}
	
	public Shop findShopByShopName(String name){
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Shop where shopName =:sname");
		   query.setString("sname",name);
		   Shop shop = (Shop)query.uniqueResult();
		   session.close();
       return shop;
	}
/*
	public static void main(String[] args){
		try {
			List<Shop> shops = Factory.newShopDao().getTopShop();
			shops.forEach( s -> System.out.println(s) );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	@Override
	public List<Shop> getTopShops() throws SQLException {
		mySql.Connect();
		ResultSet rs = mySql.Select("select o.sid from orderdetail od join `order` o on od.oid = o.id group by o.sid order by count(*) desc limit 10 ");
		List<Integer> ids = new ArrayList<Integer>();
		while(rs.next()){
			ids.add(rs.getInt(1));
		}
		mySql.close();
		Session session = sessionFactory.openSession();
	    List<Shop> shops = new ArrayList<Shop>();
	    for(int i : ids){
	    	shops.add((Shop)session.createQuery("from Shop where id = " + i).uniqueResult());
	    }
	    session.close();
		return shops;
	}

}
