package dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class mySql {
	private static String uri = "jdbc:mysql://localhost:3306/food";
	static Connection con = null;
	static Statement statement;
	public static void Connect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(uri, "root", "qwqwqw");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static ResultSet Select(String sql) 
			throws SQLException {
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    statement = con.createStatement();
	    ResultSet rs = statement.executeQuery(sql);
	    return rs;
	}
	public static int runSql(String sql) 
			throws SQLException {
	    statement = con.createStatement();
	    int result=statement.executeUpdate(sql);
	    statement.close();
	    return result;
	}
	public static void close()
	{
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		try{
			con.close();
		}catch(SQLException sqlexception)
		{
			sqlexception.printStackTrace();
		}
	}
}
