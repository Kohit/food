package dao.impl;

import java.sql.SQLException;

import org.hibernate.Query;
import org.hibernate.Session;
import vo.Admin;
import dao.AdminDao;

public class AdminImpl extends DaoImpl implements AdminDao {
	@Override

	public Admin findAdmin(String name) throws SQLException {
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Admin where name =:aname");
		   query.setString("aname",name);
		   Admin admin = (Admin) query.uniqueResult();
		   session.close();
		   return admin;
	} 
}
