package dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.OrderDetail;
import dao.OrderDetailDao;

public class OrderDetailImpl extends DaoImpl implements OrderDetailDao {
	
	@Override
	public List<OrderDetail> findOrderDetailByOrder(int oid) throws SQLException{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from OrderDetail where order.id=:oid");
		query.setLong("oid",oid);
		List<OrderDetail> list = new ArrayList<OrderDetail>();
		list = query.list();
		session.close();    
		return list;
	}
}
