package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import dao.Factory;
import vo.*;

public class ML {

	public static final int lambda = 1; 
	/**
	 * 预测指定用户对特定菜的喜好度
	 * @param fid 菜id
	 * @param uid 用户id
	 * @return 0~5的浮点数
	 */
	public static double predit(int fid, int uid){
		return getTheta(uid).dotProduct(getX(fid));
	}
	
	/**
	 * 计算预测偏差
	 * @param X 菜矩阵
	 * @param theta 用户特征向量
	 * @param y 用户评价向量
	 * @return 1/2 * SUM((theta * X(i:) - y(i))^2), where i:1~N && y(i) != 0
	 */
	public static double cost(RealMatrix X, RealVector theta, RealVector y){
		double cost = 0;
		double[] Y = y.toArray();
		for (int i = 0; i < Y.length; i++){
			if (Y[i] != 0){
				cost += Math.pow((theta.dotProduct(X.getRowVector(i)) - y.getEntry(i)), 2);
			}
		}
		cost = cost * 0.5;
		// Regularization 正则化，避免过度拟合
		cost += lambda * 0.5 * (theta.dotProduct(theta) - theta.getEntry(0) * theta.getEntry(0));
		return cost;
	}
	
	/**
	 * 为指定训练其特征向量
	 * @param X 菜矩阵
	 * @param theta 用户初始特征向量
	 * @param y 用户评价向量
	 * @return 用户特征向量
	 */
	public static RealVector train(RealMatrix X, RealVector theta, RealVector y){
		int iter = 500; // 迭代次数
		double alpha = 0.03; // 迭代速率
		double[] Y = y.toArray();
		while(iter-- > 0){
			RealVector sum = new ArrayRealVector(theta.getDimension());
			for (int i = 0; i < Y.length; i++){
				if (Y[i] != 0){
					sum = sum.add(X.getRowVector(i).mapMultiply( theta.dotProduct(X.getRowVector(i)) - Y[i] ));
				}
			}
			// 正则化 for k from 1:n, sum(k) += lambda * theta(k)
			sum = sum.getSubVector(0, 1).append(sum.getSubVector(1, sum.getDimension() - 1).add(theta.getSubVector(1, theta.getDimension() - 1).mapMultiply(lambda)));
			theta = theta.subtract((sum.mapMultiply(alpha)));
			//System.out.println(cost(X, theta, y));
		}
		return theta;
	}
	
	/**
	 * 为指定用户训练其特征向量
	 * @param uid 用户id
	 */
	public static void train(int uid) throws SQLException{
		mySql.Connect();
		double theta[] = train(getX(), getTheta(uid), getY(uid)).toArray();
		String str = "`1` = '" + theta[0] + "'";
		for( int i = 1; i < theta.length; i++ ){
			str = str + ",`" + (i + 1) + "` = '" + theta[i] + "'";
		}
		mySql.runSql("UPDATE `theta` SET " + str + " WHERE `uid`= '" + uid + "'");
		mySql.close();
	}
	
	/*
	public static void main(String args[]){
		//updateFoods();
		trainAll();
		System.out.println("ok");
	}
	*/
	
	/**
	 * 更新所有菜的特征
	 */
	public static void updateFoods(){
		try {
			List<Food> foods = Factory.newFoodDao().select(Food.class, "");
			for(Food food : foods){
				updateFood(food);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 训练所有用户的参数
	 */
	public static void trainAll(){
		try {
			List<User> users = Factory.newDao().select(User.class, "");
			for(User user : users){
				train(user.getId());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 获取表X，该表记录每样菜及其特征，每行表示一样菜
	 * @return N * M的矩阵，N为菜数，M为特征数 + 1
	 */
	public static RealMatrix getX(){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select * from x");
			rs.last();
			int count = rs.getRow();
			rs.beforeFirst();
			double x[][] = new double[count][23];
			int i = 0;
			while(rs.next()){
				x[i][0] = 1; // 增加第一列，每个值为1
				for (int j = 0; j < 22; j++){
					x[i][j + 1] = rs.getDouble(j + 2);
				}
				i++;
			}
			return new Array2DRowRealMatrix(x);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}
	
	/**
	 * 获取表Y，该表记录每个用户对每样菜的评分1-5，未评分则为0
	 * 每行表示一样菜，每列表示一个用户
	 * @return N * U的矩阵，N为菜数，U为用户数
	 */
	public static RealMatrix getY(){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select count(*) from food");
			rs.next();
			int n = rs.getInt(1);
			rs = mySql.Select("select count(*) from user");
			rs.next();
			int m = rs.getInt(1);
			// n x m n:food m:user
			double y[][] = new double[n][m];
			rs = mySql.Select("select * from comment");
			System.out.println(n + " x " + m);
			while(rs.next()){
				// food and user can't be deleted
				y[rs.getInt("fid") - 1][rs.getInt("uid") - 1] = rs.getInt("grade");
			}
			return new Array2DRowRealMatrix(y);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}
	
	/**
	 * 获取表Theta，该表记录每个用户对每个特征的喜好度
	 * @return U * M的矩阵，U为用户数，M为特征数 + 1
	 */
	public static RealMatrix getTheta(){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select * from theta");
			rs.last();
			int count = rs.getRow();
			rs.beforeFirst();
			double theta[][] = new double[count][23];
			int i = 0;
			while(rs.next()){
				for (int j = 0; j < 23; j++){
					theta[i][j] = rs.getDouble(j + 2);
				}
				i++;
			}
			return new Array2DRowRealMatrix(theta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}

	/**
	 * 获取表X中指定菜的每个特征
	 * @param fid 菜id 
	 * @return 1 * M的向量，M为特征数 + 1
	 */
	public static RealVector getX(int fid){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select * from x where fid = " + fid);
			double x[] = new double[23];
			while(rs.next()){
				x[0] = 1; // 设第一项为1
				for (int j = 0; j < 22; j++){
					x[j + 1] = rs.getDouble(j + 2); // 从1开始，跳过第一项的菜id
				}
			}
			return new ArrayRealVector(x);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}
	/**
	 * 获取表Y中指定用户的评分1-5，未评分为0
	 * @param uid 用户id
	 * @return 1 * N的向量，N为菜数
	 */
	public static RealVector getY(int uid){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select count(*) from food");
			rs.next();
			int n = rs.getInt(1);
			rs = mySql.Select("select * from comment where uid = " + uid);
			double y[] = new double[n];
			while(rs.next()){
				y[rs.getInt("fid") - 1] = rs.getInt("grade");
			}
			return new ArrayRealVector(y);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}
	
	/**
	 * 获取表Theta中指定用户对每个特征的喜好度
	 * @param uid 用户id
	 * @return 1 * M的向量，M为特征数 + 1
	 */
	public static RealVector getTheta(int uid){
		mySql.Connect();
		try {
			ResultSet rs = mySql.Select("select * from theta where uid = " + uid);
			double theta[] = new double[23];
			rs.next();
			for (int j = 0; j < 23; j++){
				theta[j] = rs.getDouble(j + 2); // 从1开始，第一项是用户id，应该跳过
			}
			return new ArrayRealVector(theta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mySql.close();
		return null;
	}
	
	public static void addUser(int uid) throws SQLException{
		mySql.Connect();
		mySql.runSql("INSERT INTO `food`.`theta` (`uid`) VALUES ('" + uid + "')");
		mySql.close();
	}
	
	public static int[] getFeatures(Food food){
		int features[] = new int[22];
		String type[] = { "快餐", "小炒", "油炸", "清蒸", "汤", "甜品", "炖", "烤" };
		String taste[] = { "清淡", "辣", "酸" }; 
		// “普通”以及“其他” 不计入分类 
		int i = 0;
		for (i = 0; i < type.length; i++){
			if (food.getType().equals(type[i])){
				features[i] = 1;
			}
		}
		for (i = 0; i < taste.length; i++){
			if (food.getTaste().contains(taste[i]))
				features[i + type.length] = 1;
		}
		String source = food.getIngredients();
		if (source.contains("饭") || source.contains("米") )
			features[9] = 1;
		if (source.contains("椒"))
			features[10] = 1;
		if (source.contains("醋"))
			features[11] = 1;
		if (source.contains("小麦") || source.contains("粉") || source.contains("面") )
			features[12] = 1;
		if (source.contains("猪") || source.contains("肉") || source.contains("牛") || source.contains("骨") )
			features[13] = 1;
		if (source.contains("鱼"))
			features[14] = 1;
		if (source.contains("海") || source.contains("虾") || source.contains("蟹") || source.contains("蚌") || source.contains("鱿") || source.contains("蛤") || source.contains("贝"))
			features[15] = 1;
		if (source.contains("蛋"))
			features[16] = 1;
		if (source.contains("菜") || source.contains("西红柿") || source.contains("萝卜") || source.contains("菇") || source.contains("茄") || source.contains("瓜") || source.contains("果"))
			features[17] = 1;
		if (source.contains("鸡肉") || source.contains("鸡腿") || source.contains("鸡翅") || source.contains("鸡柳") || source.contains("鸭"))
			features[18] = 1;
		if (source.contains("豆腐") || source.contains("豆") || source.contains("薯"))
			features[19] = 1;
		if (source.contains("奶油") || source.contains("芝士") )
			features[20] = 1;
		if (source.contains("火腿") || source.contains("培根") || source.contains("腌") || source.contains("腊"))
			features[21] = 1;

		return features;
	}

	public static void addFood(Food food) throws SQLException{
		mySql.Connect();
		int features[] = getFeatures(food);
		String str = "'" + food.getId() + "'";
		for( int i = 0; i < 22; i++ ){
			str = str + ",'" + features[i] + "'";
		}
		mySql.runSql("INSERT INTO `food`.`x` VALUES (" + str + ")");
		mySql.close();
	}
	
	public static void updateFood(Food food) throws SQLException{
		mySql.Connect();
		int features[] = getFeatures(food);
		String str = "`1` = '" + features[0] + "'";
		for( int i = 1; i < 22; i++ ){
			str = str + ",`" + (i + 1) + "` = '" + features[i] + "'";
		}
		mySql.runSql("UPDATE `food`.`x` SET " + str + " WHERE `fid`= '" + food.getId() + "'");
		mySql.close();
	}
}
