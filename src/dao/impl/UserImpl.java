package dao.impl;

import vo.Food;
import vo.User;

import java.sql.SQLException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.UserDao;

public class UserImpl extends DaoImpl implements UserDao {

	public int add(User user) throws SQLException{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int id = (int)session.save(user);
		tx.commit();
		session.close();
		ML.addUser(id);
		return id;
	}
	
	@Override
	public User findUser(String name) throws SQLException{
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from User where name =:uname");
		   query.setString("uname",name);
		   User user = (User)query.uniqueResult();
		   session.close();
		   return user;
	
	}
	
}
