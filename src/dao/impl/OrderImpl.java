package dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.Order;
import dao.Factory;
import dao.OrderDao;

public class OrderImpl extends DaoImpl implements OrderDao {

	@Override
	public List<Order> findOrdersByShop(int sid) throws SQLException{
	 	   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Order where shop.id = :sid");
		   query.setLong("sid",sid);
		   List<Order> orders = query.list();
		   orders.forEach(o -> {
			   Query q = session.createQuery("from OrderDetail where order.id=:oid");
				q.setLong("oid", o.getId());
				o.setOrderDetail(q.list());
		   });
		   session.close();    
		return orders;
	}

	@Override
	public List<Order> findOrdersByUser(int uid) throws SQLException{
	   Session session = sessionFactory.openSession();
	   Query query = session.createQuery("from Order where user.id = :uid");
	   query.setLong("uid",uid);
	   List<Order> orders = query.list();
	   orders.forEach(o -> {
		   Query q = session.createQuery("from OrderDetail where order.id=:oid");
			q.setLong("oid", o.getId());
			o.setOrderDetail(q.list());
	   });
	   session.close();    
		return orders;
	}

	@Override
	public int addWithOrdetDetails(Order order) throws SQLException {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int id = (int)session.save(order);
		order.setId(id);
		order.getOrderDetail().forEach(o -> {
			o.setOrder(order);
			session.save(o);
		});
		tx.commit();
		session.close();
		return id;
	}

}
