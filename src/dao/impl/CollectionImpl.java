package dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import vo.Collection;
import dao.CollectionDao;

public class CollectionImpl extends DaoImpl implements CollectionDao {

	@Override
	public List<Collection> findCollections(int uid) throws SQLException {
		   Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Collection where user.id =:uid");
		   query.setLong("uid",uid);
		   List<Collection> list = new ArrayList<Collection>();   
		   list= query.list();
		   session.close();
		return list;
	}

	@Override
	public Collection findCollection(int uid, int fid) throws SQLException {
		// TODO Auto-generated method stub
		  Session session = sessionFactory.openSession();
		   Query query = session.createQuery("from Collection where user.id =:uid and food.id =:fid");
		   query.setLong("uid",uid);
		   query.setLong("fid", fid);
		   Collection co = null;
		   co = (Collection) query.uniqueResult();
		   session.close();
		return co;
	}

}
