package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.Food;
import vo.MyStat;
import dao.Factory;
import dao.FoodDao;

public class FoodImpl extends DaoImpl implements FoodDao {
	
	public int add(Food food) throws SQLException{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int id = (int)session.save(food);
		tx.commit();
		session.close();
		food.setId(id);
		ML.addFood(food);
		return id;
	}

	public void updateFood(Food food) throws SQLException{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(food);
		tx.commit();
		session.close();
		ML.updateFood(food);
	}
	
	@Override
	public Food getFood(int fid, int uid) throws SQLException{
	   Session session = sessionFactory.openSession();
	   Query query = session.createQuery("from Food where id =:fid");
	   query.setLong("fid",fid);
	   Food food = (Food)query.uniqueResult();   
	   food.setSale(getSale(fid));
	   food.setFavor(getFavor(fid, uid));
	   food.setScore(getScore(fid));
	   session.close();
	   return food;
	}

	@Override
	public List<Food> findFoodsByName(String name, int uid) throws SQLException{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Food where name like :fname");
		query.setString("fname","%"+name+"%");
		List<Food> foods = query.list();
		for (Food f : foods){
		   f.setSale(getSale(f.getId()));
		   f.setFavor(getFavor(f.getId(), uid));
		   f.setScore(getScore(f.getId()));
		}
		session.close();
		return foods;
	}

	@Override
	public List<Food> findFoodsByShop(int sid, int uid) throws SQLException{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Food where shop.id = :sid");
		query.setLong("sid", sid);
		List<Food> foods = query.list();
		for (Food f : foods){
		   f.setSale(getSale(f.getId()));
		   f.setFavor(getFavor(f.getId(), uid));
		   f.setScore(getScore(f.getId()));
		}
		session.close();
		return foods;
	}

	@Override
	public int getSale(int fid) throws SQLException{
		Session session = sessionFactory.openSession();
		Query query;
		query=session.createQuery("select count(*) from OrderDetail where food.id = :fid");
		query.setLong("fid", fid);
		Long number =(Long) query.uniqueResult();
		int count = number.intValue();
		session.close();
		return count;
	}

	@Override
	public float getFavor(int fid, int uid) throws SQLException{
		if (uid <= 0) return 0;
		return (float)(ML.predit(fid, uid) * 10000000);
	}

	@Override
	public float getScore(int fid) throws SQLException{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("select avg(grade) from Comment where food.id = :fid");
		query.setLong("fid", fid);
		Double number =(Double) query.uniqueResult();	
		
		if(number== null)
			return 0;
		
		float score = number.floatValue();
		session.close();
		return score;
	}

	@Override
	public List<Food> select(String option, int uid) throws SQLException{
	    Session session = sessionFactory.openSession();
	    Query query = session.createQuery("from Food "+option+"");
	    List<Food> foods = query.list();
	    session.close();
		for (Food f : foods){
		   f.setSale(getSale(f.getId()));
		   f.setFavor(getFavor(f.getId(), uid));
		   f.setScore(getScore(f.getId()));
		}
   	   
		return foods;
	}

	@Override
	public MyStat getMyStat(int uid) throws SQLException {
		// TODO Auto-generated method stub
		String taste[] = { "普通", "清淡", "辣", "酸" };
		MyStat myStat = new MyStat();
		mySql.Connect();
		ResultSet rs = mySql.Select("select type from orderdetail as o left join food as f on o.fid = f.id group by f.type order by count(*) desc limit 1");
		rs.next();
		myStat.setType(rs.getString(1));
		rs = mySql.Select("select sum(price) from orderdetail od join `order` o on od.oid = o.id where o.uid = " + uid);
		rs.next();
		myStat.setPrice(rs.getDouble(1));
		rs = mySql.Select("select count(*) from orderdetail od join `order` o on od.oid = o.id where o.uid = " + uid);
		rs.next();
		myStat.setBuy(rs.getInt(1));
		mySql.close();
		myStat.setId(uid);
		myStat.setFood(getMostTaste(uid, 1));
		myStat.setTaste(getMostTaste(uid, 0));
		return myStat;
	}
/*
	public static void main(String[] args){
		try {
			MyStat m = Factory.newFoodDao().getMyStat(1);
			System.out.println("buy:" + m.getBuy() + " price:" + m.getPrice() + " type:" + m.getType() + " taste:" + m.getTaste() + " food:" + m.getFood());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	@Override
	public String getMostType() throws SQLException {
		// TODO Auto-generated method stub
		mySql.Connect();
		ResultSet rs = mySql.Select("select type from orderdetail as o left join food as f on o.fid = f.id group by f.type order by count(*) desc limit 1");
		rs.next();
		String mostType = rs.getString(1);
		mySql.close();
		return mostType;
	}

	@Override
	public String getMostTaste(int uid, int type) throws SQLException {
		// TODO Auto-generated method stub
		mySql.Connect();
		ResultSet rs = null;
		if (type == 0){
			if (uid <= 0)
				rs = mySql.Select("select taste from orderdetail as o left join food as f on o.fid = f.id");
			else
				rs = mySql.Select("select taste from `order` od join food.orderdetail as o on od.id = o.oid  left join food as f on o.fid = f.id where uid = " + uid);
		}else
			rs = mySql.Select("select ingredients from `order` od join food.orderdetail as o on od.id = o.oid  left join food as f on o.fid = f.id where uid = " + uid);
		List<String> tastes = new ArrayList<String>();
		while(rs.next()){
			String str = rs.getString(1);
			for (String s : str.split(";"))
				tastes.add(s);
		}
		Map<String, Integer> m = new HashMap<String, Integer>();
		for(String s: tastes){
			if (m.get(s) != null){
				m.put(s, m.get(s) + 1);
			}else
				m.put(s, 1);
		}
		List<pare> taste = new ArrayList<pare>();
		for(String key : m.keySet()){
			pare p = new pare();
			p.count = m.get(key);
			p.key = key;
			taste.add(p);
		}
		taste.sort((p1, p2) -> p2.count - p1.count);
		if (taste == null) return "";
		String mostTaste = taste.get(0).key;
		mySql.close();
		return mostTaste;
	}
	class pare{
		int count;
		String key;
	}

	@Override
	public List<Food> getTopSaleFoods() throws SQLException {
		// TODO Auto-generated method stub
		mySql.Connect();
		ResultSet rs = mySql.Select("select fid from OrderDetail group by fid order by count(*) desc limit 10");
		List<Integer> ids = new ArrayList<Integer>();
		while(rs.next()){
			ids.add(rs.getInt(1));
		}
		mySql.close();
		Session session = sessionFactory.openSession();
	    List<Food> foods = new ArrayList<Food>();
	    for(int i : ids){
	    	foods.add((Food)session.createQuery("from Food where id = " + i).uniqueResult());
	    }
	    session.close();
		for (Food f : foods){
			   f.setSale(getSale(f.getId()));
			   f.setFavor(getFavor(f.getId(), -1));
			   f.setScore(getScore(f.getId()));
			}
	   	   
		return foods;
	}

	@Override
	public List<Food> getTopScoreFoods() throws SQLException {
		List<Food> foods = select("", -1);
	    foods = foods.stream().sorted((f1, f2) -> (int)(f2.getScore() * 100 - f1.getScore() * 100 )).limit(10).collect(Collectors.toList());
		return foods;
	}


}
