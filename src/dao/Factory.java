package dao;

import dao.impl.AdminImpl;
import dao.impl.CollectionImpl;
import dao.impl.CommentImpl;
import dao.impl.DaoImpl;
import dao.impl.FoodImpl;
import dao.impl.OrderDetailImpl;
import dao.impl.OrderImpl;
import dao.impl.ShopImpl;
import dao.impl.UserImpl;

public class Factory {
	public static Dao newDao(){
		return new DaoImpl();
	}
	public static UserDao newUserDao(){
		return new UserImpl();
	}
	public static AdminDao newAdminDao(){
		return new AdminImpl();
	}
	public static ShopDao newShopDao(){
		return new ShopImpl();
	}
	public static OrderDao newOrderDao(){
		return new OrderImpl();
	}
	public static OrderDetailDao newOrderDetailDao(){
		return new OrderDetailImpl();
	}
	public static FoodDao newFoodDao(){
		return new FoodImpl();
	}
	public static CommentDao newCommentDao(){
		return new CommentImpl();
	}
	public static CollectionDao newCollectionDao(){
		return new CollectionImpl();
	}
}
