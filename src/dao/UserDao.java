package dao;

import java.sql.SQLException;

import vo.User;

public interface UserDao extends Dao {
	
	public int add(User user) throws SQLException;
	/**
	 * 根据用户名查找用户
	 * @param name
	 * @return
	 */
	public User findUser(String name) throws SQLException;
}
