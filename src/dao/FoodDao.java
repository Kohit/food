package dao;

import java.sql.SQLException;
import java.util.List;

import vo.Food;
import vo.MyStat;

public interface FoodDao extends Dao {
	/**
	 * 根据菜id获取菜，包含菜的销量、喜好度、评分
	 * @param fid 菜id
	 * @param uid 用户id
	 * @return
	 */
	public Food getFood(int fid, int uid) throws SQLException;

	public int add(Food food) throws SQLException;
	
	public void updateFood(Food food) throws SQLException;
	/**
	 * 根据菜名模糊查找
	 * @param name 菜名
	 * @param uid 用户id
	 * @return
	 */
	public List<Food> findFoodsByName(String name, int uid) throws SQLException;
	
	/**
	 * 根据店铺id查找所有菜
	 * @param sid 店铺id
	 * @param uid 用户id
	 * @return
	 */
	public List<Food> findFoodsByShop(int sid, int uid) throws SQLException;
	
	/**
	 * 计算指定菜的销量
	 * @param fid 菜id
	 * @return
	 */
	public int getSale(int fid) throws SQLException;
	
	/**
	 * 计算指定用户对指定菜的喜好度
	 * @param fid 菜id
	 * @param uid 用户id
	 * @return
	 */
	public float getFavor(int fid, int uid) throws SQLException;
	
	/**
	 * 计算指定菜的得分
	 * @param fid 菜id
	 * @return
	 */
	public float getScore(int fid) throws SQLException;
	
	/**
	 * 按条件查找食物
	 * 示例：名字包含'豆腐'的食物，option可以为"where name like '豆腐'"
	 * @param option 条件
	 * @param uid 用户id
	 * @return
	 */
	public List<Food> select(String option, int uid) throws SQLException;
	
	/**
	 * 统计用户信息
	 * @param uid 用户id
	 * @return
	 * @throws SQLException
	 */
	public MyStat getMyStat(int uid) throws SQLException;
	
	/**
	 * 查找已销售的菜中出现最多的菜类型
	 * @return
	 * @throws SQLException
	 */
	public String getMostType() throws SQLException;
	
	/**
	 * 查找已销售的菜中出现最多的口味或用料，可以指定用户
	 * @param uid 用户id -1表示不指定
	 * @param type 0 查找口味， 1查找用料
	 * @return
	 * @throws SQLException
	 */
	public String getMostTaste(int uid, int type) throws SQLException;
	
	/**
	 * 查找销量前10的菜
	 * @return
	 * @throws SQLException
	 */
	public List<Food> getTopSaleFoods() throws SQLException;
	
	/**
	 * 查找评价前10的菜
	 * @return
	 * @throws SQLException
	 */
	public List<Food> getTopScoreFoods()  throws SQLException;
	
}
