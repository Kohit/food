package dao;

import java.sql.SQLException;

import tools.ResultSet;
import vo.Admin;

public interface AdminDao extends Dao {
	/**
	 * 根据管理员账号名查找
	 * @param name
	 * @return
	 */
	public Admin findAdmin(String name) throws SQLException;
}
