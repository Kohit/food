package dao;

import java.sql.SQLException;
import java.util.List;

import tools.ResultSet;

public interface Dao {
	
	/**
	 * 插入新对象
	 * @param entity
	 */
	public int add(Object entity) throws SQLException;
	
	/**
	 * 删除指定对象
	 * @param entityClass 对象类名 如User.class
	 * @param entityid 对象ID
	 */
	public <T> void delete(Class<T> entityClass, Object entityid) throws SQLException; 
	
	/**
	 * 更新对象
	 * @param entity
	 */
	public void update(Object entity) throws SQLException;
	
	/**
	 * 根据ID查找对象
	 * @param entityClass 对象类名 如User.class
	 * @param entityid 对象ID
	 * @return
	 */
	public <T> T find(Class<T> entityClass, Object entityid) throws SQLException; 
	
	/**
	 * 返回一组数据，起点位置为firstIndex,长度为length,并根据option筛选或排序
	 * @param entityClass 对象类名 如User.class
	 * @param firstIndex 起始位置
	 * @param length 长度
	 * @param option 筛选或排序条件
	 * @return
	 */
	public <T> ResultSet<T> select(Class<T> entityClass, int firstIndex, int length, String option) throws SQLException;
	
	/**
	 * 返回一组数据，起点位置为firstIndex,长度为length
	 * @param entityClass 对象类名 如User.class
	 * @param firstIndex 起始位置
	 * @param length 长度
	 * @return
	 */
	public <T> ResultSet<T> select(Class<T> entityClass, int firstIndex, int length) throws SQLException;
	
	/**
	 * 返回一组数据，根据条件进行筛选
	 * 示例：查找User表中name为'kk'的数据，option可以为"where name = 'kk'"
	 * @param entityClass 对象类名 如User.class
	 * @param option 筛选或排序条件
	 * @return
	 */
	public <T> List<T> select(Class<T> entityClass, String option) throws SQLException;
}
