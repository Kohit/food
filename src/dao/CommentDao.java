package dao;

import java.sql.SQLException;
import java.util.List;

import vo.Comment;

public interface CommentDao extends Dao {

	/**
	 * 根据菜id查找所有评论
	 * @param fid 菜id
	 * @return
	 */
	public List<Comment> findCommentsByFood(int fid) throws SQLException;
	
	/**
	 * 根据用户id查找所有评论
	 * @param uid 用户
	 * @return
	 */
	public List<Comment> findCommentsByUser(int uid) throws SQLException;
	
	public Comment findComment(int uid, int fid) throws SQLException;
	

}
