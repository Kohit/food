package dao;

import java.sql.SQLException;
import java.util.List;

import vo.OrderDetail;

public interface OrderDetailDao extends Dao {
	/**
	 * 根据订单id查找所有订单详细
	 * @param oid
	 * @return
	 */
	public List<OrderDetail> findOrderDetailByOrder(int oid) throws SQLException;
}
