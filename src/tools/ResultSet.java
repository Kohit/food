package tools;

import java.util.List;

/**
 * 保存分页后的数据
 * */

public class ResultSet<T> {
	private List<T> list;
	private Long totalRecord; // 查找结果总记录数
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public Long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		this.totalRecord = totalRecord;
	}
}
