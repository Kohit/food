package tools;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Tools {
	
	/**
	 * 返回字串的十六进制MD5编码格式字串
	 * @param str
	 * @return
	 */
	public static String getMD5(String str){
	    char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7',  
	            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }; 
        MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        md5.update(str.getBytes());
        byte[] code = md5.digest();
    // toHexString  
        StringBuilder sb = new StringBuilder(code.length * 2);  
        for (int i = 0; i < code.length; i++) {  
            sb.append(hexChar[(code[i] & 0xf0) >>> 4]);  
            sb.append(hexChar[code[i] & 0x0f]);  
        }  
		return sb.toString();
	}
	
	public static <T> List<T> Paging(List<T> list,int pageNumber,int count){
		List<T> li = new ArrayList<>();
		for(int i=count*pageNumber;i<count*(pageNumber+1);i++){
			li.add(list.get(i));
		}
		return li;
	}
}
