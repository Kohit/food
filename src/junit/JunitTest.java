package junit;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vo.*;
import tools.*;
import dao.*;
/**
 * 各接口示例
 * 注意：由于接口尚未实现，以下方法可以无法正确执行
 * @author FongYoung
 *
 */
public class JunitTest {
	/**
	 * 添加用户名为'foo'，密码为'123'的新用户
	 * @throws SQLException 
	 */
	public void addUser() throws SQLException{
		User foo = new User();
		foo.setName("foo");
		// 密码先转为MD5编码
		foo.setPasswd(Tools.getMD5("123"));
		// 往数据库插入数据
		Factory.newUserDao().add(foo);
	}
	
	/**
	 * 获取用户名为'foo'的用户
	 * @throws SQLException 
	 */
	public void getUser() throws SQLException{
		User foo = Factory.newUserDao().findUser("foo");
		System.out.println("用户名：" + foo.getName() + "\n密码：" + foo.getPasswd());
	}
	
	/**
	 * 获取用户foo的所有收藏
	 * @throws SQLException 
	 */
	public void getCollection() throws SQLException{
		User foo = Factory.newUserDao().findUser("foo");
		List<Collection> coo = Factory.newCollectionDao().findCollections(foo.getId());
		for( Collection c : coo ){
			System.out.println(c);
		}
	}
	
	/**
	 * 获取用户foo的所有订单
	 * @throws SQLException 
	 */
	public void getOrder() throws SQLException{
		User foo = Factory.newUserDao().findUser("foo");
		List<Order> ors = Factory.newOrderDao().findOrdersByUser(foo.getId());
		// 遍历每个订单
		for (Order or : ors){
		/*
			System.out.println("订单号：" + or.getId() + " 用户：" + or.getUser().getName());
			// 输出订单详情
			for (OrderDetail od : or.getOrderDetail()){
				System.out.println("菜名：" + od.getFood().getName());
			}
			*/
		}
	}
	
	/**
	 * 用模糊查找食物名为'麻婆豆腐'的所有食物
	 * 用筛选的方法查找名字包含'豆腐'且口味为麻辣的食物，并按价格从高到低排序
	 * 用传统方法对第一步查找的食物按价格从低到高排序
	 * 用lambda方法对------------按销量从高到低排序
	 * @throws SQLException 
	 */
	public void getFood() throws SQLException{
		User foo = Factory.newUserDao().findUser("foo");
		List<Food> mpdf = Factory.newFoodDao().findFoodsByName("麻婆豆腐", foo.getId());
		List<Food> fod = Factory.newFoodDao().select("where name like '豆腐' and taste='麻辣' orderby price desc", foo.getId());
		// 对mpdf进行排序
		Collections.sort(mpdf, new Comparator<Food>(){
			@Override // 按价格从低到高排序
			public int compare(Food a, Food b) {
				return (int)(a.getPrice() - b.getPrice());
			}
		});
		// lambda写法,同时改为按销量从高到低排序
		mpdf.sort((a, b) -> b.getSale() - a.getSale());			
	}
}
