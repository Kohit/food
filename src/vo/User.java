package vo;

public class User {
		private int id;
		private String name;
		private String passwd;
	    private String realName;
	    private String phone;
	    private String address;
	    private String email;
	    private String zip;
	   
		public String toString(){
	    	return String.format("[id:%d][name:%s][passwd:%s][realName:%s][phone:%s][address:%s][email:%s][zip:%s]",
	    			id, name, passwd, realName, phone, address, email,zip);
	    }
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPasswd() {
			return passwd;
		}
		public void setPasswd(String passwd) {
			this.passwd = passwd;
		}
		public String getRealName() {
			return realName;
		}
		public void setRealName(String realName) {
			this.realName = realName;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
	    public String getZip() {
			return zip;
		}
		public void setZip(String zip) {
			this.zip = zip;
		}
}
