package vo;

import java.util.List;

public class FoyerData {
	private long total;
	private List<Food> foods;
	private long pages;
	private int page;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}
	public void setPages(long pages) {
		this.pages = pages;
	}
	public long getPages(){
		return pages;
	}
}
