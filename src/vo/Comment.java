package vo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
public class Comment {
	private int id;
	private User user;
	private Food food;
	private String time;
	private String content;
	private int grade;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}

	public String getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
	//	SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.time = time.toString();
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public void setTime() {
		this.time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		
	}

}
