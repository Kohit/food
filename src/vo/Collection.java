package vo;


import java.sql.Timestamp;

public class Collection {
	private int id;
	private User user;
	private Food food;
	private String time;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	 public String getTime(){
	    	return time;
    }
	public void setTime(Timestamp time){
//		SimpleDateFormat fm = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			this.time= time.toString();
	}

}
