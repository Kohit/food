package vo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Order {
	private int id;
	private User user;
	private Shop shop;
	private List<OrderDetail> orderDetails; 
	private String time;  // format yyyy-MM-dd HH:mm:ss
	private int state;
	private String address;
	private String phone;
	private String note;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    public String getTime(){
    	return time;
    }
	public void setTime(Timestamp time){
		this.time= time.toString();
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public List<OrderDetail> getOrderDetail() {
		return orderDetails;
	}
	public void setOrderDetail(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
	public void setTime() {
		 this.time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
	}
}
