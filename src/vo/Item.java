package vo;

// 购物车的每个量
public class Item {
	private int count; // 数量
	private Food food; // 菜
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	
}
