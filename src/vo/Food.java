package vo;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Food {
	private int id;
	private Shop shop;
	private List<Comment> comments;
	private String name;
	private float price;
	private int time;
	private int sale; //销量
	private float favor; // 喜好度
	private float score; // 得分
	private String description;
	private String taste;
	private String type;
	private String ingredients;
	private int state;
	private String img;
	private String releaseTime;
	
	public String toString(){
		return String.format("[id:%d][name:%s][time:%d][type:%s][taste:%s][ingredients:%s][description:%s][img:%s][releaseTime:%s][state:%d]",
    			id, name, time, type, taste, ingredients, description, img, releaseTime, state);
		   
	}
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(Timestamp releaseTime) {
	//	SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.releaseTime = releaseTime.toString();
	}
	public void setReleaseTime(){
		this.releaseTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTaste() {
		return taste;
	}
	public void setTaste(String taste) {
		this.taste = taste;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIngredients() {
		return ingredients;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getSale() {
		return sale;
	}
	public void setSale(int sale) {
		this.sale = sale;
	}
	public float getFavor() {
		return favor;
	}
	public void setFavor(float favor) {
		this.favor = favor;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public List<Comment> getComments(){
		return comments;
	}
	public void setComments(List<Comment> comments){
		this.comments = comments;
	}


}
