package action;

import vo.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class food extends ActionSupport {
	private int id; // 菜id
	private String result; // ajax返回值
	private Food food;
	private boolean collected; // 用户已收藏标志
	// 根据id找到菜，并判断是否已收藏
	public String execute(){
		try {
			User user = 
					(User) ServletActionContext.getRequest().getSession().getAttribute("user");
			int uid = -1;
			if(user != null){
				uid = user.getId();
				Collection co = Factory.newCollectionDao().findCollection(user.getId(), id);
				if (co != null) collected = true;
			}		
			food = Factory.newFoodDao().getFood(id, -1);
			food.setComments(Factory.newCommentDao().findCommentsByFood(food.getId()));
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}
	
	// 将菜加入购物车
	// 如能完成操作，则result内容为json格式的"success"，否则为"error"，下同
	public String buy(){
		List<Item> cart = (List<Item>) ServletActionContext.getRequest()
				.getSession().getAttribute("cart");
		if (cart == null)
			cart = new ArrayList<Item>();
		try {
			Item item = new Item();
			item.setCount(1);
			item.setFood(Factory.newFoodDao().find(Food.class, id));
			cart.add(item);
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("cart",cart);
		Map<String, String>m = new HashMap<String, String>();
		m.put("res", "success");
		setResult(JSONObject.fromObject(m).toString());
		return SUCCESS;
	}
	
	// 将菜加入用户收藏（如用户已登录）
	public String doCollect(){
		User user = 
			(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		Map<String, String>m = new HashMap<String, String>();
		if(user !=null){
			try {
				Collection collection = new Collection();
				collection.setFood(Factory.newFoodDao().find(Food.class, id));
				System.out.println(collection.getFood());
				collection.setUser(user);
				Factory.newCollectionDao().add(collection);				
			} catch (Exception e) {
				e.printStackTrace();
				return ERROR;
			}
			m.put("res", "success");
		}else{
			m.put("res", "error");
		}
		setResult(JSONObject.fromObject(m).toString());
		return SUCCESS;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public boolean isCollected() {
		return collected;
	}

	public void setCollected(boolean collected) {
		this.collected = collected;
	}

}
