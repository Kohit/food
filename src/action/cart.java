package action;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import vo.Item;
import vo.Order;

public class cart extends ActionSupport {
	private int id; // 菜id
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String result;
	// 前台从session获取菜信息，此处无需操作
	public String execute(){
		return SUCCESS;
	}
	
	// 将菜从购物车中删除
	public String rmBuy(){
		List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
		for(int i=0;i<cart.size();i++){
			Item item = cart.get(i);
			if(item.getFood().getId() == id){
				cart.remove(i);
				break;
			}
		}
		Map<String, String>m = new HashMap<String, String>();
		m.put("res", "success");
		setResult(JSONObject.fromObject(m).toString());
		return SUCCESS;
	}
	
	// 将菜的数量减一
	public String minNum(){
		List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
		for(int i=0;i<cart.size();i++){
			if(cart.get(i).getFood().getId() == id){
				if (cart.get(i).getCount() > 1)
				cart.get(i).setCount(cart.get(i).getCount()-1);
				break;
			}
		}
		Map<String, String>m = new HashMap<String, String>();
		m.put("res", "success");
		setResult(JSONObject.fromObject(m).toString());
		return SUCCESS;
	}
	// 加一
	public String addNum(){
		List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
		
		for(int i=0;i<cart.size();i++){
			if(cart.get(i).getFood().getId() == id){
				cart.get(i).setCount(cart.get(i).getCount()+1);
				break;
			}
		}
		Map<String, String>m = new HashMap<String, String>();
		m.put("res", "success");
		setResult(JSONObject.fromObject(m).toString());
		return SUCCESS;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}