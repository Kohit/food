package action;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class myOrders extends ActionSupport {
	private int id;
	private List<Order> orders;
	private String from;
	private int page;
	private int pages;
	private int total;
	private static final int pageSize = 8;
	private Shop shop;
	private String q;
	// 获取商家所有订单
	public String execute(){
		if (page == 0) page = 1;
		shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			from = "myOrders";
			return ERROR;
		}
		try {
			orders = Factory.newOrderDao().findOrdersByShop(shop.getId());
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		if (q == null) q = "";
		if (StringUtils.isNumeric(q))
			orders = orders.stream().filter(o -> String.valueOf(o.getId()).contains(q)).collect(Collectors.toList());
		else
			orders = orders.stream().filter(o -> {
				for(OrderDetail od : o.getOrderDetail()){
					if (od.getFood().getName().contains(q))
						return true;
				}
				return false;
			}).collect(Collectors.toList());
		total = orders.size();
		setPages(total / pageSize);
		if (total % pageSize != 0) setPages(getPages() + 1);
		orders = orders.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
		return SUCCESS;
	}
	
	private String chState(int state){
		Shop shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			from = "myOrders";
			return ERROR;
		}
		try {
			Order order = Factory.newOrderDao().find(Order.class, id);
			if (order != null && order.getShop().getId() == shop.getId()){
				order.setState(state);
				Factory.newOrderDao().update(order);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}
	
	// 将指定订单state改为1
	public String ship(){
		return chState(1);
	}
	// 将指定订单state改为3
	public String close(){
		return chState(3);
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public static int getPagesize() {
		return pageSize;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		try {
			this.q = new String(q.getBytes("ISO8859-1"),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
