package action;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import tools.Tools;
import vo.Admin;
import vo.Comment;
import vo.CommentData;
import vo.Food;
import vo.Order;
import vo.Shop;
import vo.User;
import vo.AjaxData;

public class admin extends ActionSupport {
	private int id; // 菜id或评论id或店铺id
	private String result;// ajax返回值
	private Food food;
	private String from;
	private int page;
	private int pages;
	private int total;
	private static final int pageSize = 20;
	private String q;
	private List<Shop> shops;
	private String passwd;
	
	
	public String adComment(){
		Admin admin =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		if (admin == null){
			from = "adComment";
			return ERROR;
		}
		return SUCCESS;
	}
	public String adShop(){
		Admin admin =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		if (admin == null){
			from = "adShop";
			return ERROR;
		}
		if (page == 0) page = 1;
		
		try {
			shops = Factory.newShopDao().select(Shop.class, "");
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		if (q == null) q = "";
			shops = shops.stream().filter(o -> String.valueOf(o.getId()).contains(q)).collect(Collectors.toList());
		
		total = shops.size();
		setPages(total / pageSize);
		if (total % pageSize != 0) setPages(getPages() + 1);
		shops = shops.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
		return SUCCESS;
	}
	// 根据id精确查找菜，将该菜所有评论转为json格式，存在result中
	public String fetchComment(){
		if (page == 0) page = 1;
		CommentData data = new CommentData();
		Admin admin =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		if (admin == null){
			from = "adComment";
			data.setResult("deny");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		try {
			food = Factory.newFoodDao().find(Food.class, id);
			if (food != null){
				List<Comment> coms = Factory.newCommentDao().findCommentsByFood(id);
				if (coms != null){
					coms = coms.stream().filter(c -> c.getContent() != null && !c.getContent().trim().equals("")).collect(Collectors.toList());
					total = coms.size();
					setPages(total / pageSize);
					if (total % pageSize != 0) setPages(getPages() + 1);
					coms = coms.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
				}
				food.setComments(coms);
				data.setResult("success");
				data.setFood(food);
				data.setPage(page);
				data.setPages(pages);
				data.setTotal(total);
			}else{
				data.setResult("not found");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}
	// 根据id删除评论
	public String deleteComment(){
		Map<String, String>m = new HashMap<String, String>();
		Admin admin =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		if (admin == null){
			from = "adComment";
			return "deny";
		}
		
		try{
			Comment co = Factory.newCommentDao().find(Comment.class, id);
			if (co != null){
				co.setContent("");
				Factory.newCommentDao().update(co);
				m.put("res", "success");
				
			}else
				m.put("res", "error");
		}catch(Exception e){
			e.printStackTrace();
		}
		result = JSONObject.fromObject(m).toString();
		return SUCCESS;
	}
	private String chState(int state){
		Map<String, String>m = new HashMap<String, String>();
		Admin admin =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		if (admin == null){
			from = "adComment";
			return "deny";
		}
		try{
			Shop shop = Factory.newShopDao().find(Shop.class, id);
			if (shop != null && shop.getState() != state){
				shop.setState(state);
				Factory.newShopDao().update(shop);
				m.put("res", "success");
			}else
				m.put("res", "error");
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		result = JSONObject.fromObject(m).toString();
		return SUCCESS;
	}
	public String doPermit(){
		return chState(1);
	}
	public String doDeny(){
		return chState(0);
	}
	
	public String doChAdPasswd(){
		Admin ad =
				(Admin) ServletActionContext.getRequest().getSession().getAttribute("admin");
		AjaxData data = new AjaxData();
		if (ad == null){
			data.setResult("deny");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		try {
			ad = Factory.newShopDao().find(Admin.class, ad.getId());
			ad.setPasswd(Tools.getMD5(passwd.trim()));
			Factory.newAdminDao().update(ad);
		} catch (SQLException e) {
			e.printStackTrace();
			data.setResult("error");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		ServletActionContext.getRequest().getSession().setAttribute("admin", ad);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public List<Shop> getShops() {
		return shops;
	}
	public void setShops(List<Shop> shops) {
		this.shops = shops;
	}
	public static int getPagesize() {
		return pageSize;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	
}
