package action;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class boughtItems extends ActionSupport {
	private int id; // 订单id
	private List<Order> orders; // 用户所有订单
	private String result;
	private String from;
	private int page;
	private int pages;
	private int total;
	private User user;
	private String q; //查询
	private static final int pageSize = 3;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String execute(){
		if (page == 0) page = 1;
		user = (User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if(user == null){
			from = "boughtItems";
			return ERROR;
		}
		try {
			orders = Factory.newOrderDao().findOrdersByUser(user.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 按搜索字串（如有）过滤
		if (q == null) q = "";
		if (StringUtils.isNumeric(q)) // 订单搜索
			orders = orders.stream().filter(o -> String.valueOf(o.getId())
					.contains(q)).collect(Collectors.toList());
		else                          // 名字搜索
			orders = orders.stream().filter(o -> {
				for(OrderDetail od : o.getOrderDetail()){
					if (od.getFood().getName().contains(q))
						return true;
				}
				return false;
			}).collect(Collectors.toList());
		// 计算分页信息
		total = orders.size();
		pages = total / pageSize;
		if (total % pageSize != 0) pages++;
		// 分页
		orders = orders.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
		return SUCCESS;
	}
	
	private String chState(int state){
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if(user == null){
			from = "boughtItems";
			return ERROR;
		}
		try {
			Order order = Factory.newOrderDao().find(Order.class, id);
			if (order != null && order.getUser().getId() == user.getId()){
				order.setState(state);
				Factory.newOrderDao().update(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
		
	}
	// 修改指定订单的状态为2
	public String confirmBuy(){
		return chState(2);
	}
	// 修改指定订单的状态为3
	public String returnItem(){
		return chState(3);
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public static int getPagesize() {
		return pageSize;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		try {
			this.q = new String(q.getBytes("ISO8859-1"),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
