package action;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.struts2.ServletActionContext;
import org.hibernate.procedure.internal.Util.ResultClassesResolutionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class myFoods extends ActionSupport {
	private List<Food> foods; 
	private int id;
	private String from;
	private int page;
	private int pages;
	private int total;
	private static final int pageSize = 10;
	private Shop shop;
	private String q;
	// 找到商家所有菜单
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String execute(){
		if (page == 0) page = 1;
		shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			from = "myFoods";
			return ERROR;
		}
		try {
			
			foods = Factory.newFoodDao().findFoodsByShop(shop.getId(), -1);
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		if (q == null) q = "";
			foods = foods.stream().filter(f -> f.getName().contains(q)).collect(Collectors.toList());
		total = foods.size();
		setPages(total / pageSize);
		if (total % pageSize != 0) setPages(getPages() + 1);
		foods = foods.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
		return SUCCESS;
	}
	// 将指定id的菜单状态改为0
	public String rmFood(){
		Shop shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			from = "myFoods";
			return ERROR;
		}
		try {
			Food food = Factory.newFoodDao().find(Food.class, id);
			if (food != null) {
				food.setState(0);
				Factory.newFoodDao().update(food);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}		
		return SUCCESS;
	}
	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}
	public static int getPagesize() {
		return pageSize;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		try {
			this.q = new String(q.getBytes("ISO8859-1"),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
