package action;

import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import tools.Tools;

public class regist extends ActionSupport {
	private Help help;
	private User user;
	private Shop shop;
	
	public String execute(){
		return SUCCESS;
	}
	// 用户注册
	public String doRegist(){
		//System.out.println(user);
		help = new Help();
		String option;
		try {
			if(Factory.newUserDao().findUser(user.getName()) != null){
				help.setName("用户名已存在");
				return ERROR;
			}
			option = "where email = '"+user.getEmail() + "'";
			if(Factory.newUserDao().select(User.class, option).size() > 0){
			//	System.out.println(Factory.newUserDao().select(User.class, option));
				help.setEmail("邮箱地址已被注册");
				return ERROR;
			}
			user.setPasswd(Tools.getMD5(user.getPasswd().trim()));
			Factory.newUserDao().add(user);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("user",user);
		
		return SUCCESS;
	}
	public String shopRegist(){
		return SUCCESS;
	}
	// 商家注册
	public String doShopRegist(){
	//	System.out.println(shop);
		help = new Help();
		String option;
		boolean ok = false;
		try {
			if(Factory.newShopDao().findShop(shop.getName()) != null){
				help.setName("用户名已存在");
				ok = true;
			}
			option = "where email = '"+shop.getEmail() + "'";
			if(Factory.newShopDao().select(Shop.class, option).size() > 0){
				help.setEmail("邮箱地址已被注册");
				ok = true;
			}
			option = "where shopName = '"+shop.getShopName() + "'";
			if(Factory.newShopDao().select(Shop.class, option).size() > 0){
				help.setShopName("店铺名以存在");
				ok = true;
			}
			option = "where phone = '"+shop.getPhone() + "'";
			if(Factory.newShopDao().select(Shop.class, option).size() > 0){
				help.setPhone("电话号码已被使用");
				ok = true;
			}
			option = "where idNumber = '"+shop.getIdNumber().trim() + "'";
			if(Factory.newShopDao().select(Shop.class, option).size() > 0){
				help.setIdNumber("证件号已被使用");
				ok = true;
			}
			if(ok == false){
				shop.setPasswd(Tools.getMD5(shop.getPasswd().trim()));
				Factory.newShopDao().add(shop);
			}
			else
				return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("shop",shop);
		
		return SUCCESS;
	}
	public Help getHelp() {
		return help;
	}
	public void setHelp(Help help) {
		this.help = help;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
}
