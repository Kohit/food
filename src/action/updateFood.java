package action;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import vo.*;

public class updateFood extends ActionSupport{
	private int id;
	private Food food;
	private Help help;
	private File image;        // 上传的图片
	private String imageFileName; // 上传的图片名
	private String imageContentType; // 图片的扩展名
	private String from;
	private String result;
	
	// 根据id获取菜信息
	public String execute(){
		Shop shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			setFrom("updateFood?id=" + id);
			return ERROR;
		}
		if (id > 0){
			try {
				food = Factory.newFoodDao().find(Food.class, id);
				result = JSONObject.fromObject(food).toString();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}
	public String fetchFood(){
		return execute();
	}
	// 根据菜信息更新
	public String doUpdateFood(){
		System.out.println(image == null);
		System.out.println(food.getId() + " img:" + image + " name:" + food.getName());
		Shop shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		help = new Help();
		AjaxData data = new AjaxData();
		if (shop == null){
			data.setResult("deny");
			setResult(JSONObject.fromObject(data).toString());
			return SUCCESS;
		}
		if (shop.getState() == 0){
			help.setName("用户尚未通过审核！");
			data.setResult("error");
			data.setHelp(help);
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		try {
			List<Food> fods = Factory.newFoodDao().findFoodsByName(food.getName(), -1);
			if (fods.size() > 0 && fods.get(0).getId() != food.getId() ){
				help.setName("菜名已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (food.getType() == null || food.getType().trim().equals("")){
			help.setType("类型不能为空");
			data.setResult("error");
			data.setHelp(help);
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		if (food.getTaste() == null || food.getTaste().trim().equals("")){
			help.setTaste("口味不能为空");
			data.setResult("error");
			data.setHelp(help);
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		food.setShop(shop);
		food.setReleaseTime();
		food.setState(1);
		if (food.getId() <= 0){
			int fid = 0;
			try {
				fid = Factory.newFoodDao().add(food);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			food.setId(fid);
		}
		if (image != null){
			int index = imageFileName.indexOf(".");
			String format = imageFileName.substring(index + 1);
			imageFileName = food.getId() + "." + format;
			String path = ServletActionContext.getServletContext().getRealPath("/img"); 
			File file = new File(path);
			if (file.exists()){
				try {
					FileUtils.copyFile(image, new File(file, imageFileName));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				food.setImg(imageFileName);
			}
		}
		try {
			Food old = Factory.newFoodDao().find(Food.class, food.getId());
			if (old != null && food.getImg() == null) food.setImg(old.getImg());
			Factory.newFoodDao().update(food);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		data.setHelp(help);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Food getFood() {
		return food;
	}
	
	public void setFood(Food food) {
		this.food = food;
	}
	public Help getHelp() {
		return help;
	}
	public void setHelp(Help help) {
		this.help = help;
	}
	public File getImage() {
		return image;
	}
	public void setImage(File image) {
		this.image = image;
	}
	public String getImageFileName() {
		return imageFileName;
	}
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	public String getImageContentType() {
		return imageContentType;
	}
	public void setImageContentType(String imageContentType) {
		this.imageContentType = imageContentType;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}
