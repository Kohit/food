package action;

import tools.Tools;
import vo.*;

import java.sql.SQLException;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class shopInfo extends ActionSupport {
	private int id;
	private Shop shop;
	private Help help;
	private String result;

	public String execute(){
		Shop shop =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		if (shop == null){
			return ERROR;
		}
		return SUCCESS;
	}
	
	// 修改店铺信息
	public String doChShopInfo(){
		Shop sh =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		help = new Help();
		AjaxData data = new AjaxData();
		if (sh == null){
			data.setResult("deny");
			setResult(JSONObject.fromObject(data).toString());
			return SUCCESS;
		}
		shop.setState(sh.getState());
		Shop s1 = null;
		try {
			s1 = Factory.newShopDao().findShopByShopName(shop.getShopName());
			if (s1 != null && s1.getId() != shop.getId()){
				help.setShopName("店铺名已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
			List<Shop> s2 = null;
			s2 = Factory.newShopDao().select(Shop.class, "where email = '" + shop.getEmail() + "'");
			if (s2.size() > 0 && s2.get(0).getId() != shop.getId()){
				help.setEmail("邮箱已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
			s2 = Factory.newShopDao().select(Shop.class, "where phone = '" + shop.getPhone() + "'");
			if (s2.size() > 0 && s2.get(0).getId() != shop.getId()){
				help.setPhone("电话已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
			s2 = Factory.newShopDao().select(Shop.class, "where idNumber = '" + shop.getIdNumber() + "'");
			if (s2.size() > 0 && s2.get(0).getId() != shop.getId()){
				help.setIdNumber("身份证号已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			shop.setPasswd(sh.getPasswd().trim());
		try {
			Factory.newShopDao().update(shop);
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("shop", shop);
		data.setHelp(help);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}
	public String doChShopPasswd(){
		Shop sh =
				(Shop) ServletActionContext.getRequest().getSession().getAttribute("shop");
		AjaxData data = new AjaxData();
		if (sh == null){
			data.setResult("deny");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		sh.setPasswd(Tools.getMD5(shop.getPasswd().trim()));
		try {
			shop = Factory.newShopDao().find(Shop.class, sh.getId());
			shop.setPasswd(sh.getPasswd());
			Factory.newUserDao().update(shop);
		} catch (SQLException e) {
			e.printStackTrace();
			data.setResult("error");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		ServletActionContext.getRequest().getSession().setAttribute("shop", shop);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Help getHelp() {
		return help;
	}

	public void setHelp(Help help) {
		this.help = help;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}
