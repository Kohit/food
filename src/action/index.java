﻿package action;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import vo.Food;
import vo.FoyerData;
import vo.Shop;
import vo.User;
import dao.Factory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.*;
public class index extends ActionSupport{
	public static int pageSize = 30; // 每页的商品数
	private String search; // 搜索字串
	private int page; // 页码
	private int type; // 菜类
	private String taste; // 菜口味,不同口味以','隔开
	private int minPrice; // 最低价格
	private int maxPrice; // 最高价格
	private int sort;  // 排序方式
	private String result; // 用于ajax方式传输的菜列表
	private String methods; // 请求方式
	private List<Food> foods; // 用于get方式传输的菜列表
	private Shop shop; // 店铺
	private int id; //店铺id
	String types[] = { "", "快餐", "小炒", "油炸", "清蒸", "汤", "甜品", "炖", "烤", "其它" };
	private long total; // 满足条件的菜总数
	private long pages; // 页数
	/*排序方式
	 * 0 喜好度 (登录用户)
	 * 1 销量 (未登录用户)
	 * 2 评价
	 * 3 价格升
	 * 4 价格降
	 * 
	 * */

	// 主页店铺通用
	public String execute(){
		// 检查用户是否登录，若未登录，查询时uid = -1
		// 按照以上列出的条件筛选菜单
		// 将菜单分页
		if (page <= 0) page = 1;
		int uid = -1;
		if (search != null && (methods == null || !methods.equals("post"))){
			try {
				search = new String(search.getBytes("ISO8859-1"),"utf-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("user");
		//System.out.println(sort + " page:" + page + " type:" + type + " taste:" + taste);
		if(user != null)
			uid = user.getId();
		else if (sort == 0) sort = 1;
		try {
			if (id == 0)
				foods = Factory.newFoodDao().select("where state = 1" , uid);
			else{
				shop = Factory.newShopDao().find(Shop.class, id);
				foods = Factory.newFoodDao().findFoodsByShop(id, uid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//foods.forEach(f -> System.out.println( f.getName() + " type:" + f.getType() + " taste:" + f.getTaste() + " price:" + f.getPrice() + " sale:" + f.getSale() + " score:" + f.getScore()));
		if (foods != null){
			Stream<Food> fs = foods.stream();
			
			// 关键字过滤
			if (search != null && search != "")
				fs = foods.stream().filter( f -> f.getName().contains(search));
			
			// 按种类过滤
			if (type > 0 && type < 10){
				fs = fs.filter(f -> f.getType().contains((types[type])));
			}
			
			// 按口味过滤
			if (taste != null && !taste.equals("") && !taste.equals("综合")){
				fs = fs.filter(f -> {
					Matcher matcher =  Pattern.compile("[" + taste.replace(";", "|") + "]").matcher(f.getTaste());
					return matcher.matches();
				});
			}
			
			// 按价格过滤
			if (minPrice > 0)
				fs = fs.filter(f -> f.getPrice() >= minPrice);
			if (maxPrice > 0 && maxPrice >= minPrice)
				fs = fs.filter(f -> f.getPrice() <= maxPrice);
			
			// 排序
			switch(sort){
				case 0: fs = fs.sorted((f1, f2) -> {
					if (f2.getFavor() == f1.getFavor()) return f2.getSale() - f1.getSale();
					return (int)(f2.getFavor() - f1.getFavor());
				}); break;
				case 1: fs = fs.sorted((f1, f2) -> f2.getSale() - f1.getSale()); break;
				case 2: fs = fs.sorted((f1, f2) -> (int)(f2.getScore() - f1.getScore())); break;
				case 3: fs = fs.sorted((f1, f2) -> (int)(f1.getPrice() - f2.getPrice())); break;
				case 4: fs = fs.sorted((f1, f2) -> (int)(f2.getPrice() - f1.getPrice())); break;
				default: break;
			}
			foods = fs.collect(Collectors.toList());
			total = foods.size();
			pages = total / pageSize;
			if (total % pageSize != 0)
				pages++;
			// 分页
			foods = foods.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
		}
		
		//foods.forEach(f -> System.out.println( f.getName() + " type:" + f.getType() + " taste:" + f.getTaste() + " price:" + f.getPrice() + " sale:" + f.getSale() + " score:" + f.getScore()));
		// 检查请求方式
		// 若为post，则将结果改为json格式，赋给result并return "ajax"
		// 否则return "success"
		if (methods != null && methods.equals("post")){
			FoyerData data = new FoyerData();
			data.setTotal(total);
			data.setPages(pages);
			data.setFoods(foods);
			data.setPage(page);
			result = JSONObject.fromObject(data).toString();
			//System.out.println(result);
			return "ajax";
		}
		else
			return SUCCESS;
	}
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		if (page == 0)
			page = 1;
		this.page = page;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTaste() {
		return taste;
	}

	public void setTaste(String taste) {
		if (taste == null) taste = "";
		this.taste = taste;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMethods() {
		return methods;
	}

	public void setMethods(String methods) {
		this.methods = methods;
	}
	public List<Food> getFoods() {
		return foods;
	}

	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public long getPages() {
		return pages;
	}

	public void setPages(long pages) {
		this.pages = pages;
	}
	
}
