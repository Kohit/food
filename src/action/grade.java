package action;

import vo.*;

import java.sql.SQLException;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import dao.impl.ML;

// 用户评价页
public class grade extends ActionSupport {
	private int id; // 菜id
	private Food food;
	private int grade; // 评分1-5
	private String comment; // 评论，可为空
	private String from;
	
	// 根据id找到菜
	public String execute(){
		User user =
				(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if (user == null){
			setFrom("grade?id=" + id);
			return ERROR;
		}
		try {
			food = Factory.newFoodDao().find(Food.class, id);
			Comment co = Factory.newCommentDao().findComment(user.getId(), food.getId());
			comment = co.getContent();
		} catch (SQLException e) {
			e.printStackTrace();
			return ERROR;
		}
		
		return SUCCESS;
	}
	// 根据传入的参数，生成评论
	public String doGrade(){
		User user = 
			(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		Comment co = null;
		try {
			co = Factory.newCommentDao().findComment(user.getId(), id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		co.setContent(comment);
		co.setGrade(grade);
		try {
			Factory.newCommentDao().update(co);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ML.train(user.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Food getFood() {
		return food;
	}
	public void setFood(Food food) {
		this.food = food;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
