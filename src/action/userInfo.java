package action;

import java.sql.SQLException;
import java.util.List;

import tools.Tools;
import vo.*;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class userInfo extends ActionSupport {
	private User user; // 通过参数传入的新的用户信息
	private Help help;
	private String from;
	private String result;
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	// 将user设为session中的user
	// 未登录返回"error"
	public String execute(){
		User user = 
			(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if(user == null){
			setFrom("userInfo");
			return ERROR;
		}
		return SUCCESS;
	}
	
	// 将传入的user跟session中的比较，更新信息
	// 如有错误，将错误信息传入help,并返回"error"
	public String doChUserInfo(){
		User u =
				(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		help = new Help();
		AjaxData data = new AjaxData();
		if (u == null){
			setFrom("userInfo");
			data.setResult("deny");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		List<User> u1 = null;
		try {
			// 校验邮箱是否已存在
			u1 = Factory.newUserDao().select(User.class, "where email = '" + user.getEmail() + "'");
			if (u1.size() > 0 && u1.get(0).getId() != user.getId()){
				help.setEmail("邮箱已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
			u1 = Factory.newShopDao().select(User.class, "where phone = '" + user.getPhone() + "'");
			if (u1.size() > 0 && u1.get(0).getId() != user.getId()){
				help.setPhone("电话已存在");
				data.setResult("error");
				data.setHelp(help);
				result = JSONObject.fromObject(data).toString();
				return SUCCESS;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		user.setPasswd(u.getPasswd().trim());
		// 更新用户并返回成功结果
		try {
			Factory.newUserDao().update(user);
		} catch (SQLException e) {
			e.printStackTrace();
			return SUCCESS;
		}
		// 
		ServletActionContext.getRequest().getSession().setAttribute("user", user);
		data.setHelp(help);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}
	public String doChUserPasswd(){
		User u =
				(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		AjaxData data = new AjaxData();
		if (u == null){
			setFrom("userInfo");
			data.setResult("deny");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		u.setPasswd(Tools.getMD5(user.getPasswd().trim()));
		try {
			user = Factory.newUserDao().find(User.class, u.getId());
			user.setPasswd(u.getPasswd());
			Factory.newUserDao().update(user);
		} catch (SQLException e) {
			e.printStackTrace();
			data.setResult("error");
			result = JSONObject.fromObject(data).toString();
			return SUCCESS;
		}
		ServletActionContext.getRequest().getSession().setAttribute("user", user);
		data.setResult("success");
		result = JSONObject.fromObject(data).toString();
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Help getHelp() {
		return help;
	}

	public void setHelp(Help help) {
		this.help = help;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}
