package action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;
import dao.impl.ML;

public class checkout extends ActionSupport {
	private List<Order> orders; // 订单
	private List<OrderDetail> orderDetails; // 订单详细
	private int[] fid;
	private int[] amount;
	private int[] shopIds;
	private String[] notes; // 备注
	private boolean finished; // 支付完成标志
	// 修改用户配送信息
	private String address;
	private String phone;
	private String realName;
	private String result;

	// 从购物车取出商品，加入到order中
	public String check(){
		Map<String, String>m = new HashMap<String, String>();
		m.put("res", "success");
		User user = (User) ServletActionContext.getRequest()
				.getSession().getAttribute("user");
		if(user == null){
			m.put("res", "login");
		}
			List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
			// 购物车为空
			if (cart == null || cart.size() <= 0){
				m.put("res", "empty");
			}
			
			setResult(JSONObject.fromObject(m).toString());
			
		return SUCCESS;		
	}
	
	// 加载前台页面，在此之后表单信息将重新传入
	public String execute(){
		// 将菜按店铺分组，生成多个订单

		Map<String, String>m = new HashMap<String, String>();
		User user = (User) ServletActionContext.getRequest()
				.getSession().getAttribute("user");
		if(user == null){
			m.put("res", "login");
			setResult(JSONObject.fromObject(m).toString());
			return ERROR;
		}
			List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
			// 购物车为空
			if (cart == null || cart.size() <= 0){
				m.put("res", "empty");
				setResult(JSONObject.fromObject(m).toString());
				return ERROR;
			}
		mkOrders(null);
		return SUCCESS;
	}
	
	private void mkOrders(List<Item> Items){
		User user = (User) ServletActionContext.getRequest()
				.getSession().getAttribute("user");
		List<Item> cart = (List<Item>) ServletActionContext.getRequest().getSession().getAttribute("cart");
		if (Items != null) cart = Items;
		if (cart == null || user == null) return ;
		// 将商品按店铺分组
		Map<Integer, List<Item>> m = new HashMap<Integer, List<Item>>();
		for(Item i : cart){
			if (m.get(i.getFood().getShop().getId()) != null){
				m.get(i.getFood().getShop().getId()).add(i);
			}else{
				List<Item> items = new ArrayList<Item>();
				items.add(i);
				m.put(i.getFood().getShop().getId(), items);
			}
		}
		// 生成订单信息
		orders = new ArrayList<Order>();
		for (int sid : m.keySet()){
			Order order = new Order();
			order.setUser(user);
			List<OrderDetail> ods = new ArrayList<>();
			for(Item item : m.get(sid)){
				OrderDetail od = new OrderDetail();
				od.setFood(item.getFood());
				od.setAmount(item.getCount());
				od.setPrice(item.getFood().getPrice());
				ods.add(od);
			}
			order.setShop(ods.get(0).getFood().getShop());
			order.setOrderDetail(ods);
			order.setAddress(user.getAddress());
			if (Items != null){
				for(int i = 0; i < shopIds.length; i++){
					if (shopIds[i] == sid){
						order.setNote(notes[i]);
						break;
					}
				}
			}
			order.setPhone(user.getPhone());
			order.setState(0);
			order.setTime();
			orders.add(order);
		}
		return ;
	}
	
	// 修改用户以上三个配送信息
	public String chShipInfo(){
		AjaxData data = new AjaxData();
		User user = 
				(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if(user != null)
		{
			try {
				user = Factory.newUserDao().findUser(user.getName());
				user.setAddress(address);
				user.setRealName(realName);
				user.setPhone(phone); 
				Factory.newUserDao().update(user);
			} catch (SQLException e) {
				data.setResult("error");
				data.setMessage("电话号码已存在");
				return SUCCESS;
			}
		}
		result = JSONObject.fromObject(data).toString();
		ServletActionContext.getRequest().getSession().setAttribute("user", user);
		return SUCCESS;
	}
	
	// 利用传入的参数生成最终订单order
	public String mkOrder(){
		User user = (User) ServletActionContext.getRequest()
				.getSession().getAttribute("user");
		List<Item> items = new ArrayList<Item>();
		for (int i = 0; i < fid.length; i++){
			Item item = new Item();
			try {
				Food food = Factory.newFoodDao().find(Food.class, fid[i]);
				item.setFood(food);
				item.setCount(amount[i]);
				items.add(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mkOrders(items);
		return SUCCESS;
	}
	
	// 支付操作，如支付完成，则finished为true, 仅限站内支付
	public String pay(){
		finished = true; // 此处省略实际支付操作
		return SUCCESS;
	}
	
	// 若finished为true，将生成的order插入数据库
	public String finish(){
		if(finished == true){
			User user = (User) ServletActionContext.getRequest()
					.getSession().getAttribute("user");
			try {
				for(Order order : orders){
					Factory.newOrderDao().addWithOrdetDetails(order);
					// 生成默认评价
					order.getOrderDetail().forEach( o -> {
						
						try {
							if (Factory.newCommentDao().findComment(order.getUser().getId(), o.getFood().getId()) != null ){
								return ;
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Comment co = new Comment();
						co.setUser(order.getUser());
						co.setContent("");
						co.setFood(o.getFood());
						co.setGrade(5);
						co.setTime();
						try {
							Factory.newCommentDao().add(co);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				}
				// 训练模型
				ML.train(user.getId());
				
			} catch (SQLException e) {
				e.printStackTrace();
				return ERROR;
			}
			
			// 清空购物车
			if (ServletActionContext.getRequest().getSession().getAttribute("cart") != null){
				ServletActionContext.getRequest().getSession().removeAttribute("cart");
			}
		}
		return SUCCESS;
	}
	
	
	public List<Order> getOrders() {
		return this.orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	public int[] getFid() {
		return fid;
	}

	public void setFid(int[] fid) {
		this.fid = fid;
	}

	public int[] getAmount() {
		return amount;
	}

	public void setAmount(int[] amount) {
		this.amount = amount;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int[] getShopIds() {
		return shopIds;
	}

	public void setShopIds(int[] shopIds) {
		this.shopIds = shopIds;
	}

	public String[] getNotes() {
		return notes;
	}

	public void setNotes(String[] notes) {
		this.notes = notes;
	}
	
}
