package action;

import java.sql.SQLException;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class collection extends ActionSupport {
	private List<Collection> collections;
	public List<Collection> getCollections() {
		return collections;
	}
	public void setCollections(List<Collection> collections) {
		this.collections = collections;
	}
	// 获取用户所有收藏
	public String execute(){
		User user = 
			(User) ServletActionContext.getRequest().getSession().getAttribute("user");
		if (user != null){
			try {
				collections = Factory.newCollectionDao().findCollections(user.getId());
			} catch (SQLException e) {
				e.printStackTrace();
				return ERROR;
			}
		}
		return SUCCESS;
	}
}
