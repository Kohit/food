package action;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.struts2.ServletActionContext;

import vo.*;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class statistic extends ActionSupport {
	private List<Food> topSaleFoods;
	private List<Food> topScoreFoods;
	private List<Shop> topShops;
	private String mostType;
	private String mostTaste;
	private MyStat myStat;
	
	public String execute(){
		try {
			topSaleFoods = Factory.newFoodDao().getTopSaleFoods();
			topScoreFoods = Factory.newFoodDao().getTopScoreFoods();
			topShops = Factory.newShopDao().getTopShops();
			mostType = Factory.newFoodDao().getMostType();
			mostTaste = Factory.newFoodDao().getMostTaste(-1, 0);
			User user =
					(User) ServletActionContext.getRequest().getSession().getAttribute("user");
			if (user != null){
				myStat = Factory.newFoodDao().getMyStat(user.getId());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public List<Food> getTopSaleFoods() {
		return topSaleFoods;
	}
	public void setTopSaleFoods(List<Food> topSaleFoods) {
		this.topSaleFoods = topSaleFoods;
	}
	public List<Food> getTopScoreFoods() {
		return topScoreFoods;
	}
	public void setTopScoreFoods(List<Food> topScoreFoods) {
		this.topScoreFoods = topScoreFoods;
	}
	public List<Shop> getTopShops() {
		return topShops;
	}
	public void setTopShops(List<Shop> topShops) {
		this.topShops = topShops;
	}
	public String getMostType() {
		return mostType;
	}
	public void setMostType(String mostType) {
		this.mostType = mostType;
	}
	public String getMostTaste() {
		return mostTaste;
	}
	public void setMostTaste(String mostTaste) {
		this.mostTaste = mostTaste;
	}
	public MyStat getMyStat() {
		return myStat;
	}
	public void setMyStat(MyStat myStat) {
		this.myStat = myStat;
	}
	

}
