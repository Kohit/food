package action;

import tools.Tools;
import vo.*;

import java.sql.SQLException;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.Factory;

public class login extends ActionSupport {
	private String from; // 来源
	private User user;
	private Shop shop;
	private Admin admin;
	private Help help; // 表单填写错误信息
	
	public String execute(){
		return SUCCESS;
	}
	
	// 用户登录
	public String doLogin(){
		User u = null;
		help = new Help();
		try {
			u = Factory.newUserDao().findUser(user.getName());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (u == null){
			help.setName("用户名不存在");
			return ERROR;
		}
		if (!u.getPasswd().equals(Tools.getMD5(user.getPasswd().trim()))){
			help.setPasswd("密码不正确");
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("user",u);
		if (from != null && !from.trim().equals(""))
			return from;
		return SUCCESS;
	}
	
	public String shopLogin(){
		return SUCCESS;
	}
	
	// 商家登录
	public String doShopLogin(){
		if(shop == null) return ERROR;
		Shop u = null;
		help = new Help();
		try {
			u = Factory.newShopDao().findShop(shop.getName());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (u == null){
			help.setName("用户名不存在");
			return ERROR;
		}
		if (!u.getPasswd().equals(Tools.getMD5(shop.getPasswd().trim()))){
			help.setPasswd("密码不正确");
			return ERROR;
		}
		
		ServletActionContext.getRequest().getSession().setAttribute("shop",u);
		
		if (from != null && !from.trim().equals(""))
			return from;

		return SUCCESS;
	}
	
	public String adLogin(){
		return SUCCESS;
	}
	
	// 管理员登录
	public String doAdLogin(){
		Admin u = null;
		help = new Help();
		try {
			u = Factory.newAdminDao().findAdmin(admin.getName());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (u == null){
			help.setName("用户名不存在");
			return ERROR;
		}
		if (!u.getPasswd().equals(Tools.getMD5(admin.getPasswd().trim()))){
			help.setPasswd("密码不正确");
			return ERROR;
		}
		ServletActionContext.getRequest().getSession().setAttribute("admin",u);
		
		if (from != null && !from.trim().equals(""))
			return from;

		return SUCCESS;
	}
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public Help getHelp() {
		return help;
	}
	public void setHelp(Help help) {
		this.help = help;
	}
}
