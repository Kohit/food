function validate_shop(){
            $("label.help").text("");
            // 校验邮箱
            var mail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            var email = $("#email").val();
            if (!mail.test(email)){
                $("#helpEmail").text("邮箱格式不正确");
                return false;
            }
            // 校验电话/手机
            var phone = $("#phone").val();
            var tel = /^((\d{3,4})|\d{3,4}-)?\d{8}$/;
            if (!tel.test(phone)){
                $("#helpPhone").text("电话格式不正确");
                return false;
            }
            var shopName = $("#shopName").val();
            if (shopName == "" || shopName.length > 45){
                $("#helpShopName").text("店铺名长度不能超过45,且不能为空");
                return false;
            }
            var owner = $("#ownerName").val();
            if (owner == "" || owner.length > 45){
                $("#helpOwnerName").text("姓名长度不能超过45,且不能为空");
                return false;
            }
            // 校验身份证号
            var idcard = /^\d{17}[\d|X]$|^\d{15}$/;
            if( !idcard.test($("#idNumber").val()) ){
                $("#helpIdNumber").text("身份证格式不正确");
                return false;
            }
            if ($("#address").val() == "" ){
                $("#helpAddress").text("地址不能为空");
                return false;
            }
            // 校验邮编
            var zipcode = /^\d{6}$/;
            if (!zipcode.test($("#zip").val())){
                $("#helpZip").text("邮编格式不正确");
                return false;
            }
            return true;
}
function validate_passwd(){
	            // 校验密码
            var passwd = $("#passwd").val();
            if (passwd == ""){
                $("#helpPasswd").text("密码不能为空");
                return false;
            }
            var rePasswd = $("#rePasswd").val();
            if (rePasswd != passwd){
                $("#helpRePasswd").text("密码不匹配");
                return false;
            }
            return true;
}