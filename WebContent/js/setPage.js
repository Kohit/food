	function setPage(cur, last){
		$("#paging-food").empty();
		   var str = "";
		   if (cur <= 1)
		       str = "<span class='btnpg disabled'>上一页</span>";
		   else
		       str = "<a class='btnpg prevPage' href='javascript:;'>上一页</a>"
		   if (cur == 1)
		       str += "<span class='btnpg current'>1</span>";
		   else
		       str += "<a class='btnpg' href='javascript:;'>1</a>";
		   if (cur - 3 > 1)
		       str += "<span class='btnpg abb'>...</span>";
		   if (cur - 2 > 1)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur - 2) + "</a>";
		   if (cur - 1 > 1)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur - 1) + "</a>";
		   if (cur != 1)
		       str += "<span class='btnpg current'>" + cur + "</span>";
		   if (cur + 1 < last)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur + 1) + "</a>";
		   if (cur + 2 < last)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur + 2) + "</a>";
		   if (cur + 3 < last)
		       str += "<span class='btnpg abb'>...</span>";
		   if (last > cur)
		       str += "<a class='btnpg' href='javascript:;'>" + last + "</a>";
		   if (cur >= last)
		       str += "<span class='btnpg disabled'>下一页</span>";
		   else
		       str += "<a class='btnpg nextPage' href='javascript:;'>下一页</a>";
		    $("#paging-food").append(str);
	}