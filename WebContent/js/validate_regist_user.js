    $(document).ready(function(){
        $("button").click(function(){
            $("label.help").text("");
            // 校验用户名
            var name = $("#name").val();
            if (name == "" || name.length < 3 || name.length > 10){
                $("#helpUserName").text("用户名长度为3-10位");
                 $(".alert-danger").show("slow");
                return false;
            }
            var word = /\W/;
            if (word.test(name)){
                $("#helpUserName").text("用户名只能包含字母、数字和下划线");
                 $(".alert-danger").show("slow");
                return false;
            }
            // 校验邮箱
            var mail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            var email = $("#email").val();
            if (!mail.test(email)){
                $("#helpEmail").text("邮箱格式不正确");
                 $(".alert-danger").show("slow");
                return false;
            }
            // 校验密码
            var passwd = $("#passwd").val();
            if (passwd == ""){
                $("#helpPasswd").text("密码不能为空");
                 $(".alert-danger").show("slow");
                return false;
            }
            var rePasswd = $("#rePasswd").val();
            if (rePasswd != passwd){
                $("#helpRePasswd").text("密码不匹配");
                 $(".alert-danger").show("slow");
                return false;
            }
        });
    });