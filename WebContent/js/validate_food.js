	function checkImg(obj){
		var path = obj.value;
		var len = path.length;
		var str = path.substring(len, path.lastIndexOf('.') + 1);
		var ext = "JPG,GIF,PNG,JPEG";
		if(ext.indexOf(str.toUpperCase()) < 0)  {   
		   document.getElementById("helpImg").innerText = "图片格式不正确！仅支持JPG,JPEG,GIF,PNG";
		   return;  
		}
		document.getElementById("img").src = path;
		var size = document.getElementById("setImg").files[0].size;
		if (size > 1 * 1024 * 1024){ // 3MB
			document.getElementById("helpImg").innerText = "图片大于1M";
			return;
		}
		document.getElementById("helpImg").innerText = "";
	}
	
	$(document).ready(function(){
        $("button").click(function(){
            $("label.help").text("");
            var name = $("#name").val();
            if (name == "" || name.length > 45){
                $("#helpName").text("菜名长度不能超过45,且不能为空");
                return false;
            }
            // 校验价格
            var price = $("#price").val();
            var pri = /^\d{1,10}(\.\d{1,2})?$/;
            if (!pri.test(price)){
                $("#helpPrice").text("价格格式不正确");
                return false;
            }
            // 校验制作时间
            var ptime = /^\d{1,3}$/;
            if( !ptime.test($("#time").val()) ){
                $("#helpTime").text("时间格式不正确");
                return false;
            }
            var sour = /^([\u2E80-\u9FFF]|[a-zA-Z]|[\u2E80-\u9FFF];|[a-zA-Z];)+$/;
            if (!sour.test($("#source").val())){
                $("#helpSource").text("用料只包含文字和分隔符';'");
                return false;
            }
            if ($("#helpImg").text() != "")
                return false;
        });
    });