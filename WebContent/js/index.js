   function postdata(){
		var x=$("form").serializeArray();
		$.post("index",x,function(data){
			$("#food-list.list-wrap").empty();
	    	var u = eval("("+ data +")");
	    	$.each(u, function(i, field){
			    $("#food").append(field.name + ":" + field.price + "<br>");
			});
			
		});  
	}
	$(document).ready(function(){
            $(".type .filter-list a").click(function(){
                if ( $(this).parent().hasClass("active") ){
                    $(this).parent().removeClass("active");
                    $("#type").val(0);
                }
                else{
                    $(this).parent().siblings().removeClass("active");
                    $(this).parent().addClass("active");
                    var types = new Array("综合","快餐","小炒","油炸","清蒸","汤", "甜品", "炖", "烤", "其他");
                    for (i in types){
                        if (types[i] == $(this).text()){
                            $("#type").val(i);
                            break;
                        }
                    }
                }
                postdata();
            });
            $(".taste .filter-list a").click(function(){
                if ( $(this).parent().hasClass("active") ){
                    $(this).parent().removeClass("active");
                    $("#taste").val("");
                    $(this).parent().siblings().each(function(){
                        if ($(this).hasClass("active"))
                            $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                    });
                }else{
                    $(this).parent().addClass("active");
                    $("#taste").val($(this).text());
                    if ($(this).text() == "综合"){
                        $(this).parent().siblings().each(function(){
                            if ($(this).hasClass("active"))
                                $(this).removeClass("active");
                        });
                    }else
                        $(this).parent().siblings().each(function(){
                            if ($(this).hasClass("active")){
                                if ($(this).find("a").text() == "综合")
                                    $(this).removeClass("active");
                                else
                                    $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                            }
                        });
                }
                postdata();
            });
            $(".sort .filter-list a").click(function(){
                if ( $(this).parent().hasClass("active") ){
                    $(this).parent().removeClass("active");
                    $("#sort").val(0);
                }
                else{
                    $(this).parent().siblings().removeClass("active");
                    $(this).parent().addClass("active");
                    var sorts = new Array("喜好度", "销量", "评价", "价格升", "价格降");
                    for( i in sorts ){
                        if (sorts[i] == $(this).text()){
                            $("#sort").val(i);
                            break;
                        }
                    }
                }
                postdata();
            });
		$("#btnSearch").click(function(){
			location.href="index?search=" + encodeURIComponent($("input#txtSearch").val());
		});
		$("#btnPrice").click(function(){
			postdata();
		});
		$("#pre").click(function(){
			if ($("#page").val() == "")
				$("#page").val("1");
			if ($("#page").val() <= 1) return;
			$("#page").val(parseInt($("#page").val()) - 1);
			postdata();
		});
		$("#next").click(function(){
			if ($("#page").val() == "")
				$("#page").val("1");
			if ($("#pageSize").val() == "")
				$("#pageSize").val("10");
			var pageCount = $("#totalRes").val() / $("#pageSize");
			if ($("#totalRes").val() % $("#pageSize") != 0)
				pageCount++;
			if ($("#page").val() >= pageCount) return;
			$("#page").val(parseInt($("#page").val()) + 1);
			postdata();
		});
	});