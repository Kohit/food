function postdata(){
	var x=$("form").serializeArray();
	$("#food-list .list-wrap").empty();
	$("#food-list .list-wrap").append("正在加载...");
	$.post("index",x,function(data){
		$("#food-list .list-wrap").empty();
    	var res = eval("("+ data +")");
    	$(".paging-res").html("<span class='totalPage'>" + res.pages + "</span>页/<span class='totalRes'>" + res.total + "</span>条结果");
    	setPage(parseInt(res.page), parseInt(res.pages));
    	$.each(res.foods, function(i, food){
    		var str = "<li class='list aver";
            if (i % 5 == 0 && i != 0){
                str += " new-line";
            }
                
            str += "'><div class='img'><a href='food?id=" + food.id + "' target='_blank'>";
            str += "<img src='img/" + food.img + "' /></a></div>";
            str += "<div class='food-info'><a class='food-name' href='food?id=" + food.id + "' target='_blank'>" + food.name + "</a></div>";
            str += "<div class='food-info'>";
            str += "<p class='food-rank'><span>评价:" + food.score + "</span>&nbsp;&nbsp;&nbsp;";
            str += "<span>销量:" + food.sale + "</span></p>";
            str += "<p class='food-price'>￥" + food.price + "</p></div></li>";
           
	    $("#food-list .list-wrap").append(str);
    	});
	});  
}
$(document).ready(function(){
        $(".type .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#type").val(0);
            }
            else{
                $(this).parent().siblings().removeClass("active");
                $(this).parent().addClass("active");
                var types = new Array("综合","快餐","小炒","油炸","清蒸","汤", "甜品", "炖", "烤", "其他");
                for (i in types){
                    if (types[i] == $(this).text()){
                        $("#type").val(i);
                        break;
                    }
                }
            }
            $("#page").val(0);
            postdata();
        });
        $(".taste .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#taste").val("");
                $(this).parent().siblings().each(function(){
                    if ($(this).hasClass("active")){
                    	if ($("#taste").val() != "")
	                        $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                    	else
                    		$("#taste").val($(this).find("a").text());
                        
                    }
                });
            }else{
                $(this).parent().addClass("active");
                $("#taste").val($(this).text());
                if ($(this).text() == "综合"){
                    $(this).parent().siblings().each(function(){
                        if ($(this).hasClass("active"))
                            $(this).removeClass("active");
                    });
                }else
                    $(this).parent().siblings().each(function(){
                        if ($(this).hasClass("active")){
                            if ($(this).find("a").text() == "综合")
                                $(this).removeClass("active");
                            else
                                $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                        }
                    });
            }
            $("#page").val(0);
            postdata();
        });
        $(".sort .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#sort").val(0);
            }
            else{
                $(this).parent().siblings().removeClass("active");
                $(this).parent().addClass("active");
                var sorts = new Array("喜好度", "销量", "评价", "价格升序", "价格降序");
                for( i in sorts ){
                    if (sorts[i] == $(this).text()){
                        $("#sort").val(i);
                        break;
                    }
                }
            }
            $("#page").val(0);
            postdata();
        });
	$("#btnSearch").click(function(){
		location.href="shop?id=" + $('#shopId').val() + "&search=" + encodeURIComponent($("input#txtSearch").val());
		});
	$("#btnPrice").click(function(){
		$("#page").val(0);
		postdata();
	});
});

$(document).on("click", "a.btnpg", function(){
    var cur = parseInt($("#page").val());
    var last = parseInt($(".totalPage").text());
   if ($(this).hasClass("prevPage"))
       cur--;
   else if ($(this).hasClass("nextPage"))
       cur++;
   else
       cur = parseInt($(this).text());
   $("#page").val(cur);
   setPage(cur, last);
    postdata();
});
function setPage(cur, last){
	$("#paging-food").empty();
	   var str = "";
	   if (cur <= 1)
	       str = "<span class='btnpg disabled'>上一页</span>";
	   else
	       str = "<a class='btnpg prevPage' href='javascript:;'>上一页</a>"
	   if (cur == 1)
	       str += "<span class='btnpg current'>1</span>";
	   else
	       str += "<a class='btnpg' href='javascript:;'>1</a>";
	   if (cur - 3 > 1)
	       str += "<span class='btnpg abb'>...</span>";
	   if (cur - 2 > 1)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur - 2) + "</a>";
	   if (cur - 1 > 1)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur - 1) + "</a>";
	   if (cur != 1)
	       str += "<span class='btnpg current'>" + cur + "</span>";
	   if (cur + 1 < last)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur + 1) + "</a>";
	   if (cur + 2 < last)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur + 2) + "</a>";
	   if (cur + 3 < last)
	       str += "<span class='btnpg abb'>...</span>";
	   if (last > cur)
	       str += "<a class='btnpg' href='javascript:;'>" + last + "</a>";
	   if (cur >= last)
	       str += "<span class='btnpg disabled'>下一页</span>";
	   else
	       str += "<a class='btnpg nextPage' href='javascript:;'>下一页</a>";
	    $("#paging-food").append(str);
}
