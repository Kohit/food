	$(document).ready(function(){
		$("button").click(function(){
			$("#helpUserName").text("");
			$("#helpPasswd").text("");
			var name = $("#name").val();
			if (name == "" || name.length < 3 || name.length > 10){
                $("#helpUserName").text("用户名长度为3-10位");
                 $(".alert-danger").show("slow");
                return false;
            }
            var word = /\W/;
            if (word.test(name)){
                $("#helpUserName").text("用户名只能包含字母、数字和下划线");
                 $(".alert-danger").show("slow");
                return false;
            }
            var passwd = $("#passwd").val();
            if (passwd == ""){
                $("#helpPasswd").text("密码不能为空");
                 $(".alert-danger").show("slow");
                return false;
            }
              
		});
	});