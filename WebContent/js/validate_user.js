function validate_user(){
            $("label.help").text("");
            // 校验邮箱
            var mail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            var email = $("#email").val();
            if (!mail.test(email)){
                $("#helpEmail").text("邮箱格式不正确");
                return false;
            }
            // 校验电话/手机
            var phone = $("#phone").val();
            var tel = /^((\d{3,4})|\d{3,4}-)?\d{8}$/;
            if (!tel.test(phone)){
                if(phone != ""){ // 允许为空
                    $("#helpPhone").text("电话格式不正确");
                    return false;
                }
            }
            var realName = $("#realName").val();
            if (realName.length > 45){
                $("#helpShopName").text("姓名长度不能超过45");
                return false;
            }
            // 校验邮编
            var zipcode = /^\d{6}$/;
            if (!zipcode.test($("#zip").val())){
                if ($("#zip").val() != ""){ // 邮编允许为空
                    $("#helpZip").text("邮编格式不正确");
                    return false;
                }
            }
            return true;
}
function validate_passwd(){
	            // 校验密码
            var passwd = $("#passwd").val();
            if (passwd == ""){
                $("#helpPasswd").text("密码不能为空");
                return false;
            }
            var rePasswd = $("#rePasswd").val();
            if (rePasswd != passwd){
                $("#helpRePasswd").text("密码不匹配");
                return false;
            }
            return true;
}