<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>用户登录</title>
<script src="js/jquery.min.js"></script>
<script src="js/validate_login.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<style>
body{
	background:#fff;
}
</style>
</head>
<body>
	<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>用户登录</h1>
            </div>
        </div>
    </div>
	<hr />
			<div class="alert alert-danger" role="alert" style="display:none">登录失败，请检查输入</div>
	
	<div class="container">
        <div class="row" >
            <div class="col-md-7" >
            </div>
        </div>
		<div class="well pull-right col-md-4">
		<form action="doLogin" method="post">
			<c:if test="${empty(from) }">
			<input type="hidden" name="from" value="${param.from}">
			</c:if>
			<c:if test="${!empty(from) }">
			<input type="hidden" name="from" value="${from}">
			</c:if>
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" id="name" name="user.name" type="text" placeHolder="username">
				<label id="helpUserName">${help.name}</label>
			</div>
			<div class="form-group">
				<label>密码：</label>
				<input id="passwd" name="user.passwd" class="form-control" type="password" placeHolder="password">
				<label id="helpPasswd">${help.passwd}</label>
			</div>
			<div class="form-group" >
			    <button type="submit" class="btn btn-primary">sign in</button>
			</div>
			<a href="regist">register a account</a>  
		</form>
		</div>
	</div>
	  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This webSite is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div> 

</body>
</html>