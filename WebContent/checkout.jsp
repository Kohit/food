<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>结算</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("#chInfo").click(function(){
			$("#info").toggle("500");
		});
		$("#chShipInfo").click(function(){
            // 校验电话/手机
            var phone = $("#phone").val();
            var tel = /^((\d{3,4})|\d{3,4}-)?\d{8}$/;
            if (!tel.test(phone)){
                    $("#helpPhone").text("电话格式不正确");
                    return false;
            }
            // 提交数据
			var data = { address:$("#address").val(), 
					realName:$("#name").val(),
					phone:$("#phone").val()};
			$.post("chShipInfo", data, function(result){
				var res  = eval("("+ result +")");
				if (res.result == "success"){
					// 更新前台数据
					$("#shipAddress").text($("#address").val());
					$("#shipName").text($("#name").val());
					$("#shipPhone").text($("#phone").val());
				}else{
					alert(res.message);
				}
			});
			
		});
		$("#pay").click(function(){
			var address = $("#shipAddress").text();
			var name = $("#shipName").text();
			var phone = $("#shipPhone").text();
			if (address != "" && name != "" && phone != ""){
				$("form").submit();
			}else{
				alert("请先填写完整配送信息");
			}
		});
		
	});
</script>
<style type="text/css">
	.change{
	height:20px;
	width:30px;
	border:none;
	font-size:15px;
	background:#ff3e00;
	color:white;
	}
	.save{
	height:20px;
	width:30px;
	border:none;
	font-size:15px;
	background:#ff3e00;
	color:white;
	}
	.last{
	width:100%;
	text-align:right;
	}
	.lasttotal{
	margin-right:10px;
	font-size:20px;
	color:red;
	font-weight:bold;
	}
	.pay{
	height:50px;
	width:100px;
	border:none;
	font-size:20px;
	background:#ff3e00;
	color:white;
	}
</style>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

</head>
<body style="background-color:white"> 
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车<span class="cartSize">(${fn:length(sessionScope.cart)})</span></a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div style="margin:0 auto;width:80%;">
	<div id="info" style="display:none">
		<p>收货信息</p>
		<hr>
		地址：
		<input id="address" type="text" value="${user.address}" class="form-control" style="width:30%">
		<label id="helpAddress"></label><br/>
		收货人：
		<input id="name" type="text" value="${user.realName }" class="form-control"  style="width:30%">
		<label id="helpName"></label><br/>
		联系电话/手机：
		<input id="phone" type="number" value="${user.phone}" class="form-control"  style="width:30%">
		<label id="helpPhone"></label><br/>
		<button id="chShipInfo" class="save">保存</button>
	</div>
	<hr>
	<div style="width:400px;">
		<p style="font-size:30px;">确认收货信息</p>
		<p id="shipInfo" style="border:solid 1px;border-left:none;border-top:none;border-color:#ff3e00;font-size:20px">
			地址:<span id="shipAddress" >${user.address}</span><br/>
			收货人名字:<span id="shipName">${user.realName}(收)</span><br/>
			电话号码:<span id="shipPhone">${user.phone}</span>
		</p>
		<button id="chInfo" class="change">修改</button>
		
	</div>
	<br/>
	<div>
		<p style="font-size:30px;">确认订单信息</p>

		<c:set value="0" var="total"></c:set>
		<form action="mkOrder" method="post">
		<table class="table table-bordered">
			<tr>
				<th>商品信息</th><th>单价</th><th>数量</th><th>小计(元)</th>
			</tr>
		<c:forEach items="${orders}" var="order">
			<tr>
				<td>店铺：<a href="shop?id=${order.shop.id}">${order.shop.shopName}</a></td>
			</tr>
			<c:forEach items="${order.orderDetail}" var="item">
			<tr>
				<td>
					<div>
						<a href="food?id=${item.food.id }" target="_blank">
						<img src="img/${item.food.img}" width="100">
						${item.food.name }
						</a>
						<input type="hidden" name="fid" value="${item.food.id}">
					</div>
				</td>
				<td>
					${item.food.price}
				</td>
				<td>
					${item.amount }
					<input type="hidden" name="amount" value="${item.amount}">
				</td>
				<td>
					${item.food.price * item.amount}
					<c:set value="${total + item.food.price * item.amount}" var="total"></c:set>
				</td>
			</tr>
		</c:forEach>
							<tr>
				
				<td colspan="3">给卖家留言：<input type="text" name="notes"></td>
				<td><input type="hidden" name="shopIds" value="${order.shop.id}"></td>
			</tr>
			</c:forEach>
		
		</table>
		
		</form>
	</div>
	<div class="last">
	合计：<span class="lasttotal">${total}元 </span>
		<div><button id="pay" class="pay" >支付</button></div>
		
	</div>
	</div>
	  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>