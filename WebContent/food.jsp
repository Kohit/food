<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${food.name }</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("#btnSearch").click(function(){
			location.href="index?search=" + encodeURIComponent($("input#txtSearch").val());
		});
		$("#btnCart").click(function(){
			$.post("buy", $("#fid").serialize(), function(data){
				alert("已加入购物车");
				var cs = parseInt($(".cartSize").text());
				$(".cartSize").text(cs + 1);
			});
		});
		$("#btnCollect").click(function(){
			$.post("doCollect", $("#fid").serialize(),function(data){
		    	var u = eval("("+ data +")");
		    	if (u.res == "success")
		    		$("#collect").text("已收藏");
		    	else
		    		alert("收藏失败，尚未登录");
			}); 
		});
	});
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<style>
	.shopNav{
		margin-left:10px;
	}
        .container{
            width:100%;
                background: #fff;
                display: block;
              height: auto;
                padding-top: 17px;
  padding-bottom: 14px;
  background: #fff;
            
        }
                    .food-block {
  width: 1100px;
  height: auto;
  padding-left: 30px;
  padding-right: 8px;
  margin-left: 45px;
  margin-right: 27px;
  border: 1px solid #e5e9ef;
  padding-top: 24px;
  position: relative;
  padding-bottom: 14px;
                    overflow:hidden;
                    }
                
                    .col-md-6{
                        margin-left: -10px;
                       
                    }
                    .food-name{
                        font-size: 24px;
                        padding-top: 10px;
                        padding-bottom: 14px;
                    }
                    .detail-meta{
                        padding: 10px;
                        background-color: #fff2e8;
                        min-height: 30px;
                        color:#3c3c3c;
                    }
                    .detail-price{
                        width: 350px;
                        margin-bottom: 0px;
                        line-height: 24px;
                        
                    }
                    .detail-property{
                        display: inline;
                        float: left;
                        width: 60px;
                        color: #6c6c6c;
                    }
                    .detail-price-meta{
                        position: relative;
                       
                        vertical-align: middle;
                        padding-right: 5px;
                        font-size: 24px;
                        font-weight: 700;
                        font-family: Tahoma,Arial,Helvetica,sans-serif;
                        color:#f40;
                        overflow: hidden;
                    }
                    .detail-price-meta .rmb{
                        font-family: arial;
                        font-weight: 400;
                        margin-right: 4px;
                        font-style: normal;
                    }
                    .detail-price-meta .rmbnum{
                        font-family: verdana,arial;
                        font-style: normal;
                    }
                    .detail-count{
                        min-width:100px;
                        position: absolute;
                        top:64px;
                        right:50px;
                    }
                    .rate{
                        padding-right: 9px;
                        padding-left: 9px;
                        float: left;
                        text-align: center;
                        border-right: 1px solid #ffe4d0;
                    }
                    .sale{
                        padding-left: 9px;
                        text-align: center;
                        float: left;
                    }
                    .rate-count{
                        display: block;
                        font-size: 18px;
                        font-weight: 400;
                        line-height: 18px;
                    }
                    .detail{
                        font-size: 14px;
                        padding-top: 8px;
                        padding-bottom: 8px;
                        padding-left: 5px;
                    }
                    .detail-value{
                        padding-left: 5px;
                    }
                    .action{
                        padding-top: 12px;
                        padding-bottom: 12px;
                        padding-left: 5px;
                        display: inline-block;
                    }
                    .btnbuy{
                        float: left;
                        border-radius:4px;
                        border:1px solid #e5e9ef;
                        background:#ff6600;
                        margin-right:10px;
                        text-align:center;
                        width:138px;
                        height:38px;
                        cursor:pointer;
                        line-height:38px;
                        color:#fff;
                        font-size: 18px;
                    }
                    .btnCol{
                        float: left;
                        border-radius:4px;
                        border:1px solid #f0cab6;
                        background:#ffe4d0;
                        margin-right:10px;
                        text-align:center;
                        width:100px;
                        height:38px;
                        cursor:pointer;
                        line-height:38px;
                        color:#e5511d;
                        font-size: 18px;
                    }
                    .btnbuy:visited{
                    	color:#fff;
                    }
                    .btnbuy:hover{
                        background: #ff3300;
                        color:#fff;
                    }
                    .btnCol:hover{
                        background: #ffe3a0;
                        color:#e5511d;
                    }
                    .food-img{
                     	height:400px;
				        width:400px;
				        overflow:hidden;
				        text-align:center;
				        vertical-align:middle;
				        display: table-cell;
                    }
                    .food-img img{
                    vertical-align:middle;
                   		width:100%;
                    }
                    .comment-board{
                         width: 1140px;
                        height: auto;
                        padding-left: 60px;
                        padding-right: 20px;
                        padding-top: 24px;
                        position: relative;
                        padding-bottom: 14px;
                        overflow:hidden;
                    }
                    .comment-list{
                        border-bottom: 1px solid #e5e9ef;
                        padding-top: 20px;
                        padding-bottom: 10px;
                    }
                    .title{
                        color:#222;
                        line-height: 18px;
                        padding-bottom: 8px;
                         font-size: 14px;
                    }
                    .content{
                        line-height: 20px;
                        font-size: 14px;
                        overflow: hidden;
                        word-wrap: break-word;
                        padding-bottom: 8px;
                    }
                    .time{
                        color:#aaa;
                        line-height: 14px;
                    }
                    body{
                    	background: #fff;
                    }
                    	    input, button{
	    	box-sizing:inherit;
	   			 }
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
            |
            <span class="shopNav">
            <a href="shop?id=${food.shop.id}">${food.shop.shopName }</a>
            </span>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
		<div class="top">
    	<div class="searchbar">
        	<div class="searchbox">
            	<span class="icon"></span>
            	<input id="txtSearch" type="text" placeholder="搜索">
	        </div>
    	    <button id="btnSearch" >搜索</button>
    	</div>
	</div>
	<div class="container">
            <div class="food-block">
        <div>
            <div class="col-md-5" >
            	<div class="food-img">
            		<img src="img/${food.img }">
            	</div>
            </div>
        </div>
		<div class="col-md-6">
			<input type="hidden" name="id" id="fid" value="${food.id }">
                        
			<p class="food-name">${food.name }</p>
            <ul class="detail-meta">
                <li class="detail-price">
                    <span class="detail-property">价格</span>
                    <strong class="detail-price-meta">
                        <em class="rmb">¥</em>
                        <em class="rmbnum">${food.price }</em>
                    </strong>
                </li>
                <li class="detail-count">
                   
                    <div class="rate">
                        <strong class="rate-count">${food.score }</strong>
                        <span>评价</span>
                    </div>
                    <div class="sale">
                        <strong class="rate-count">${food.sale }</strong>
                        <span>销量</span>
                    </div>
                    
                </li>
            </ul>
            <ul>
                <li class="detail">
                    <span class="detail-property">制作时间</span>
                    <span class="detail-value">${food.time }分钟</span>
                </li>
                <li class="detail">
                    <span class="detail-property">类型</span>
                    <span class="detail-value">${food.type }</span>
                </li>
                <li class="detail">
                    <span class="detail-property">口味</span>
                    <span class="detail-value">${food.taste }</span>
                </li>
                <li class="detail">
                    <span class="detail-property">用料</span>
                    <span class="detail-value">${food.ingredients }</span>
                </li>
                <li class="detail">
                    <span class="detail-property">描述</span>
                    <span class="detail-value">${food.description }</span>
                </li>
            </ul>
            <div class="action">
                <a id="btnCart" href="javascript:;" class="btnbuy">加入购物车</a>
                <a id="btnCollect" class="btnCol">
                <c:choose>
				<c:when test="${collected}">
					已收藏
				</c:when>
				<c:otherwise>
                <span id="collect">加入收藏</span>
                </c:otherwise>
				</c:choose>
                </a>
            </div>
		</div>
	</div>
  </div>
<div class="comment-board">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">评论</h3>
            </div>
            <div class="panel-body">
                <ul>
                <c:forEach items="${food.comments}" var="com">
                    <li class="comment-list">
                        <div class="title">
                            ${com.user.name } <span class="badge">${com.grade }</span>
                        </div>
                        <div class="content">
                           ${com.content }
                        </div>
                        <div class="time">
                           ${com.time }
                        </div>
                    </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        </div>

</body>
</html>