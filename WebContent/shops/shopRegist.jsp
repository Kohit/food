<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商家注册</title>
<script src="js/jquery.min.js"></script>
<script src="js/validate_regist_shop.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<style>
body{
	background:#fff;
}
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
                <h1 style="padding-left:50px">商家注册</h1>
	<hr />
	<div class="container">
		<div class="alert alert-danger" role="alert" style="display:none">注册失败，请检查输入</div>
	
        <div class="col-md-3" >
        </div>
        <div class="well col-md-4">
        <form action="doShopRegist" method="post">
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" id="name" name="shop.name" type="text" placeHolder="username">
				<label class="help" id="helpUserName">${help.name}</label>
			</div>
			<div class="form-group">
				<label>邮箱：</label>
				<input class="form-control" id="email" name="shop.email" type="text" placeHolder="如abc@abc.abc">
				<label class="help" id="helpEmail">${help.email}</label>
			</div>
			<div class="form-group">
				<label>联系电话/手机：</label>
				<input class="form-control" id="phone" name="shop.phone" type="text" placeHolder="如100-1000-1000">
				<label class="help" id="helpPhone">${help.phone}</label> 
			</div>
			<div class="form-group">
				<label>店铺名：</label>
				<input class="form-control" id="shopName" name="shop.shopName" type="text" placeHolder="30位字符">
				<label class="help" id="helpShopName">${help.shopName}</label>
			</div>
			<div class="form-group">
				<label>真实姓名:</label>
				<input class="form-control" id="ownerName" name="shop.ownerName" type="text" placeHolder="">
				<label class="help" id="helpOwnerName"></label>
			</div>
			<div class="form-group">
				<label>身份证号:</label>
				<input class="form-control" id="idNumber" name="shop.idNumber" type="text" placeHolder="">
				<label class="help" id="helpIdNumber">${help.idNumber }</label>
			</div>
			<div class="form-group">
				<label>店铺地址：</label>
				<input class="form-control" id="address" name="shop.address" type="text" placeHolder="">
				<label class="help" id="helpAddress"></label>
			</div>
			<div class="form-group">
				<label>邮政编码：</label>
				<input class="form-control" id="zip" name="shop.zip" type="text" placeHolder="6位数字">
				<label class="help" id="helpZip"></label>
			</div>
			<div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="password">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="shop.passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
			<div class="form-group" >
			    <button type="submit" class="btn btn-primary">register</button>
			</div>
		</form>
	  </div>
	</div>
	
	  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>