<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>加菜</title>
<script src="js/jquery.min.js"></script>
<script src="js/validate_food.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

	<div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>更新食品信息</h1>
            </div>
        </div>
    </div>
	<hr />
	<div class="container">
        <div class="row" >
            <div class="col-md-7" >
            </div>
        </div>
		<div class="well pull-right col-md-4">	
		<form action="doUpdateFood" method="post">
			<div class="form-group">
				<label>菜ID：</label>
				<label>${food.id}</label>
			</div>
			<div class="form-group">
				<label>菜名：</label>
				<input class="form-control" id="name" name="food.name" type="text" value="${food.name}">
	            <label class="help" id="helpName">${help.name}</label>
			</div>
			<div class="form-group">
				<label>单价：</label>
				<input class="form-control" id="price" name="food.price" type="text" placeHolder="如8.5" value="${food.price}">
				<label class="help" id="helpPrice"></label>
			</div>
			<div class="form-group">
				<label>制作时间/分钟：</label>
				<input class="form-control" id="time" name="food.time" type="text" placeHolder="如20" value="${food.time}">
				<label class="help" id="helpTime"></label>
			</div>
			<div class="form-group">
				<label>描述：</label>
				<input class="form-control" id="des" name="food.description" type="text" value="${food.description}">
				<label class="help" id="helpdescription"></label>
			</div>
			<div id="taste" class="form-group">
				<label>口味：</label>
				<input type="checkbox" name="taste" checked="checked" value="普通">普通
				<input type="checkbox" name="taste" value="清淡">清淡
				<input type="checkbox" name="taste" value="辣">辣
				<input type="checkbox" name="taste" value="酸">酸
				<input type="checkbox" name="taste" value="甜">甜
			</div>
			<div class="form-group">
				<label>类型：</label>
				<select id="type" name="shop.type">
					<!--  
					<option value="1">快餐</option>
					<option value="2">小炒</option>
					<option value="3">甜点</option>
					-->
					<option value="0">全部</option>
					<option value="1">快餐</option>
					<option value="2">小炒</option>
					<option value="3">油炸</option>
					<option value="4">清蒸</option>
					<option value="5">汤</option>
					<option value="6">甜品</option>
					<option value="7">炖</option>
					<option value="8">烤</option>
					<option value="9">其他</option>					
				</select>
			</div>
			<div class="form-group">
				<label>用料：</label>
				<input class="form-control" id="source" name="shop.ingredients" type="text" placeHolder="多种用料用分号(;)隔开">
				<label class="help" id="helpSource"></label>
			</div>
			<div class="form-group">
				<label>照片：</label>
				<input id="setImg" name="img" type="file" onchange="checkImg(this)" >
			</div>
			<div class="form-group">
				<label id="helpImg">请选择图片</label>
				<img id="img" width="100px">
			</div>
			<div class="form-group">
			<button type="submit" class="btn btn-primary active">保存</button>
			</div>
		</form>
	</div>
	</div>

	<!--  
	<div class="nav">
	<a href="index">首页</a>
	<a href="shop?id=${sessionScope.shop.id }">店铺首页</a>
		<c:choose>
			<c:when test="${!empty(sessionScope.shop.id)}">
				<a href="shop?id=${sessionScope.shop.id }">${sessionScope.shop.name }</a>
			</c:when>
			<c:otherwise>
			
				<a href="login">登录/注册</a>
			</c:otherwise>
		</c:choose>
	</div>
	<form action="doUpdateFood" method="post" enctype="multipart/form-data">
		<div>
			<label>菜ID：</label>
			<input type="text" name="food.id" readonly="readonly" value="${food.id }">
		</div>
		<div>
			<label>菜名：</label>
			<input id="name" name="food.name" type="text" value="${food.name}">
            <label class="help" id="helpName">${help.name}</label>
		</div>
		<div>
			<label>单价：</label>
			<input id="price" name="food.price" type="text" placeHolder="如8.5" value="${food.price}">
			<label class="help" id="helpPrice"></label>
		</div>
		<div>
			<label>制作时间/分钟：</label>
			<input id="time" name="food.time" type="text" placeHolder="如20" value="${food.time}">
			<label class="help" id="helpTime"></label>
		</div>
		<div>
			<label>描述：</label>
			<input id="des" name="food.description" type="text" value="${food.description}">
			<label class="help" id="helpdescription"></label>
		</div>
		<div id="taste">
			<label>口味：</label>
			<input type="checkbox" name="food.taste" value="普通">普通
			<input type="checkbox" name="food.taste" value="清淡">清淡
			<input type="checkbox" name="food.taste" value="辣">辣
			<input type="checkbox" name="food.taste" value="酸">酸
			<input type="checkbox" name="food.taste" value="甜">甜
			<label>${help.taste }</label>
		</div>
		<div>
			<label>类型：</label>
			<select id="type" name="food.type">
				<option value="全部">全部</option>
				<option value="快餐">快餐</option>
				<option value="小炒">小炒</option>
				<option value="油炸">油炸</option>
				<option value="清蒸">清蒸</option>
				<option value="汤">汤</option>
				<option value="甜品">甜品</option>
				<option value="炖">炖</option>
				<option value="烤">烤</option>
				<option value="其他">其他</option>
			</select>
			<label>${help.type }</label>
		</div>
		<div>
			<label>用料：</label>
			<input id="source" name="food.ingredients" type="text" value="${food.ingredients}" placeHolder="多种用料用分号(;)隔开">
			<label class="help" id="helpSource"></label>
		</div>
		<div>
			<label>照片：</label>
			<input id="setImg" name="image" type="file" onchange="checkImg(this)" >
			<label id="helpImg"></label>
			<img id="img" src="img/${food.img }" width="100px">
		</div>
		<button type="submit">保存</button>
	</form>
	-->
</body>
</html>