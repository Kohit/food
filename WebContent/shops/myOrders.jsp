<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单管理</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/setPage.js"></script>
<script src="js/validate_shop.js"></script>
<script>
	$(document).ready(function(){
		$("#search").click(function(){
			$("#q").val($("#txtSearch").val());
			location.href="myOrders?q=" + encodeURIComponent($("#q").val());
		})
		$(".shipped").click(function(){
			if(!confirm("是否确认配送?")) return ;
			$.post("ship", {id:$(this).parents(".order-title").find("span.oid").text()});
			$(this).parents(".order-title").find("span.state").text("已配送，未签收");
			$(this).remove();
		});
		$(".revoke").click(function(){
			if(!confirm("是否确认关闭?")) return ;
			$.post("revoke", {id:$(this).parents(".order-title").find("span.oid").text()});
			$(this).parents(".order-title").find("span.state").text("交易已关闭");
			$(this).remove();
		});
		$(".order-title").click(function(){
			$(this).nextUntil(".order-title").toggle("slow");
		});
		$("a.btnpg").click(function(){
		    var cur = parseInt($("#page").val());
		    var last = parseInt($(".totalPage").text());
		   if ($(this).hasClass("prevPage"))
		       cur--;
		   else if ($(this).hasClass("nextPage"))
		       cur++;
		   else
		       cur = parseInt($(this).text());
		   $("#page").val(cur);
		   setPage(cur, last);
		   
		   location.href="myOrders?q=" + encodeURIComponent($("#q").val()) + "&page=" + $("#page").val();
		
		});
		$("#save").click(function(){
			$(".alert.alert-danger").hide("slow");
			if (validate_shop()){
				var x=$("form").serializeArray();
				$.post("doChShopInfo",x,function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    		if (res.help.email != ""){
			    			$("#helpEmail").text(res.help.email);
			    			return ;
			    		}
			    		if (res.help.phone != ""){
			    			$("#helpPhone").text(res.help.phone);
			    			return ;
			    		}
			    		if(res.help.shopName != ""){
			    			$("#helpShopName").text(res.help.shopName);
			    			return ;
			    		}
			    		if (res.help.idNumber != ""){
			    			$("#helpIdNumber").text(res.help.idNumber);
			    			return ;
			    		}
			    	}else if(res.result == "deny"){
			    		location.href = "shopLogin?from=myOrders";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
					
				}); 
			}else
				$(".alert-danger").show("slow");
		});
		$("#savePasswd").click(function(){
			$(".alert.alert-danger").hide("slow");
			if (validate_passwd()){
				$.post("doChShopPasswd",$("#rePasswd").serialize(),function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    	}else if(res.result == "deny"){
			    		location.href = "shopLogin?from=myOrders";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
				}); 
			}else
				$(".alert-danger").show("slow");
		});
	});
	
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/userCenter.css">
<style>
    .order-detail-head h5{
        width:210px;
    }
        .order-detail h5{
        width: 200px;
    }
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myOrders" target="_blank">商家中心</a>
        </div>
	</div>
	<div class="container-fluid">
        <div class="col-md-1"></div>
		<div class="col-md-2">
            <div class="title">商家中心</div>
				<ul class="left-menu">
					<li class="cur">
						<a href="myOrders"><span>我的订单</span></a>
					</li>
					<li>
	                	<a href="myFoods"><span>我的商品</span></a>
					</li>
					<li>
						<a href="#shopModal" data-toggle="modal"><span>店铺信息</span></a>
					</li>
					<li>
	                    <a href="#passwdModal" data-toggle="modal"><span>修改密码</span></a>
					</li>
				</ul>
		</div>
		<div class="col-md-8">
			<div class="s-margin">
				<input id="txtSearch" type="text" placeHolder="订单号或菜名">
				<span id="search" class="s-btn">订单搜索</span>
			</div>
			<input id="q" type="hidden">
			<input type="hidden" name="page" id="page" value="${page}">
			<input type="hidden" id="total" value="${total }">
            <div class="order">
            <c:forEach items="${orders}" var="order">
                <div class="order-title">
                    <p>订单号:<span class="oid">${order.id}</span></p>
                    <p>客户名：${order.user.name}</p>
                    <p>下单时间: ${order.time }</p>
                    <p>状态:
                    <input class="state" type="hidden" value="${order.state}">
						<span class="state">
							<c:if test="${order.state == 0}">尚未配送</c:if>
							<c:if test="${order.state == 1}">已配送，未签收</c:if>
							<c:if test="${order.state == 2}">交易完成</c:if>
							<c:if test="${order.state == 3}">交易已关闭</c:if>
						</span>
                    </p>
                    <p>操作:
						<c:if test="${order.state == 0}">		
						<a class="shipped" href="javascript:;">确认配送</a>
						<a class="revoke" href="javascript:;">取消交易</a>
						</c:if>
                    </p>
                </div>
                <div class="order-detail-head">
                    <p>商品信息</p>
                    <h2>单价</h2>
                    <h3>数量</h3>
                    <h4>实付款</h4>
                    <h5>备注</h5>
                </div>
                <c:forEach items="${order.orderDetail}" var="item">
                <div class="order-detail">
                    <dl>
                        <dt>
                            <a href="food?id=${item.food.id}" target="_blank">
                            <img width="87" height="87" src="img/${item.food.img }">
                            </a>
                        </dt>
                        <dd>
                            <a href="food?id=${item.food.id}" target="_blank">${item.food.name }</a>
                        </dd>
                    </dl>
                    <h2>${item.price}</h2>
                    <h3>${item.amount }</h3>
                    <h4>${item.price * item.amount }</h4>
                    <h5>
                    	${order.note }
                    </h5>
                </div>
                </c:forEach>
            </c:forEach>    
            </div>
			<div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                	<c:if test="${page <= 1 }">
                    <span class="btnpg disabled">上一页</span>
                    </c:if>
                    <c:if test="${page > 1 }">
                    <span class="btnpg prevPage">上一页</span>
                    </c:if>
                    <c:if test="${page == 1 }">
                    <span class="btnpg current">1</span>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <a class="btnpg" href="javascript:;">1</a>
                    </c:if>
                    <c:if test="${page - 3 > 1 }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${page - 2 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 2}</a>
                    </c:if>
                    <c:if test="${page - 1 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 1}</a>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <span class="btnpg current">${page }</span>
                    </c:if>
                    <c:if test="${page + 1 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 1}</a>
                    </c:if>
                    <c:if test="${page + 2 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 2}</a>
                    </c:if>
                    <c:if test="${page + 3 < pages }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${pages > page }">
                    <a class="btnpg" href="javascript:;">${pages}</a>
                    </c:if>
                    <c:if test="${page >= pages }">
                    <span class="btnpg disabled">下一页</span>
                    </c:if>
                    <c:if test="${page < pages }">
                    <a class="btnpg nextPage" href="javascript:;">下一页</a>
                    </c:if>
                    
                </span>
                </div>
            </div>
		</div>
	</div>
	
	
	<!-- Modal -->
<div class="modal fade" id="shopModal" tabindex="-1" role="dialog" aria-labelledby="shopLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="shopLabel">修改信息</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
      
		<form action="" onsubmit="return false;">
			<div class="form-group">
				<label>用户ID：</label>
				<input class="form-control" type="text" name="shop.id" readonly="readonly" value ="${shop.id}">
			</div>
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" name="shop.name" type="text" readonly="readonly" placeHolder="username" value="${shop.name}">
			</div>
			<div class="form-group">
				<label>邮箱：</label>
				<input class="form-control" id="email" name="shop.email" type="text" placeHolder="如abc@abc.abc" value="${shop.email}">
	            <label class="help" id="helpEmail">${help.email}</label>
			</div>
			<div class="form-group">
				<label>联系电话/手机：</label>
				<input class="form-control" id="phone" name="shop.phone" type="text" placeHolder="如100-1000-1000" value="${shop.phone }">
				<label class="help" id="helpPhone">${help.phone}</label> 
			</div>
			<div class="form-group">
				<label>店铺名：</label>
				<input class="form-control" id="shopName" name="shop.shopName" type="text" placeHolder="30位字符" value="${shop.shopName}">
				<label class="help" id="helpShopName">${help.shopName}</label>
			</div>
			<div class="form-group">
				<label>真实姓名:</label>
				<input class="form-control" id="ownerName" name="shop.ownerName" type="text" placeHolder="" value="${shop.ownerName}">
				<label class="help" id="helpOwnerName"></label>
			</div>
			<div class="form-group">
				<label>身份证号:</label>
				<input class="form-control" id="idNumber" name="shop.idNumber" type="text" placeHolder="" value="${shop.idNumber}">
				<label class="help" id="helpIdNumber"></label>
			</div>
			<div class="form-group">
				<label>店铺地址：</label>
				<input class="form-control" id="address" name="shop.address" type="text" placeHolder="" value="${shop.address}">
				<label class="help" id="helpAddress"></label>
			</div>
			<div class="form-group">
				<label>邮政编码：</label>
				<input class="form-control" id="zip" name="shop.zip" type="text" value="${shop.zip}" placeHolder="6位数字">
				<label class="help" id="helpZip"></label>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" id="save" class="btn btn-primary">保存</button>
      </div>
    </div>
  </div>
</div>
	<!-- Modal -->
<div class="modal fade" id="passwdModal" tabindex="-1" role="dialog" aria-labelledby="passwdLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="passwdLabel">修改密码</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
            <div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码不能为空">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="shop.passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
      </div>
      
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" id="savePasswd" class="btn btn-primary">保存</button>
      </div>
    </div>
  </div>
</div>
	<!-- footer -->
	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>