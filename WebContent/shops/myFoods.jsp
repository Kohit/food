<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>菜单管理</title>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/setPage.js"></script>
<script src="js/validate_shop.js"></script>
<script src="js/validate_food_2.js"></script>
<script>
	function editFood(fid){
		$(".alert").hide("slow");
		$(".help").text('');
		// 添加
		if (fid < 1){
			$("#foodLabel").text('添加菜单');
			$("#f-id").val(-1);
			$("#foodModal").modal('show');
		}else{ //修改
			$(".f-message").text('加载中...');
			$("#foodModal").modal('show');
			$.post("fetchFood",{id:fid},function(data){
				$(".f-message").text('');
				var food = eval("("+ data +")");
				$("#f-id").val(food.id);
				$("#f-name").val(food.name);
				$("#f-price").val(food.price);
				$("#f-time").val(food.time);
				$("#f-des").val(food.description);
				$("#f-source").val(food.ingredients);
			});
		}
		
	}
	$(document).ready(function(){
		$("#search").click(function(){
			$("#q").val($("#txtSearch").val());
			location.href="myFoods?q=" + encodeURIComponent($("#q").val());
		})
		$("#add").click(function(){
			if($("#state").val() == "0")
				alert("该账号尚未通过审核！");
			else
				editFood(-1);
				//location.href="updateFood?id=-1";
		});
		$(".edit").click(function(){
			editFood(parseInt($(this).parents(".order-title").find("span.id").text()));
			//location.href="updateFood?id=" + $(this).parents(".order-title").find("span.id").text();
		});
		$(".close-f").click(function(){
			if(!confirm("该操作不可逆,是否确认下架?")) return ;
			$.post("rmFood", {id:$(this).parents(".order-title").find("span.id").text()});
			$(this).parents(".order-title").find("span.state").text("已下架");
			$(this).parents().empty();
		});
		$(".order-title").click(function(){
			$(this).nextUntil(".order-title").toggle("slow");
		});
		$("a.btnpg").click(function(){
		    var cur = parseInt($("#page").val());
		    var last = parseInt($(".totalPage").text());
		   if ($(this).hasClass("prevPage"))
		       cur--;
		   else if ($(this).hasClass("nextPage"))
		       cur++;
		   else
		       cur = parseInt($(this).text());
		   $("#page").val(cur);
		   setPage(cur, last);
		   
		   location.href="myFoods?q=" + encodeURIComponent($("#q").val()) + "&page=" + $("#page").val();
		
		});
		$("#save").click(function(){
			$(".alert").hide("slow");
			if (validate_shop()){
				var x=$("#form1").serializeArray();
				$.post("doChShopInfo",x,function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    		if (res.help.email != ""){
			    			$("#helpEmail").text(res.help.email);
			    			return ;
			    		}
			    		if (res.help.phone != ""){
			    			$("#helpPhone").text(res.help.phone);
			    			return ;
			    		}
			    		if(res.help.shopName != ""){
			    			$("#helpShopName").text(res.help.shopName);
			    			return ;
			    		}
			    		if (res.help.idNumber != ""){
			    			$("#helpIdNumber").text(res.help.idNumber);
			    			return ;
			    		}
			    	}else if(res.result == "deny"){
			    		location.href = "shopLogin?from=myFoods";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
					
				}); 
			}else
				$(".alert-danger").show("slow");
		});
		$("#savePasswd").click(function(){
			$(".alert").hide("slow");
			if (validate_passwd()){
				$.post("doChShopPasswd",$("#rePasswd").serialize(),function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    	}else if(res.result == "deny"){
			    		location.href = "shopLogin?from=myFoods";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
				}); 
			}else
				$(".alert-danger").show("slow");
		});
		$("#saveFood").click(function(){
			$(".alert").hide("slow");
			if(validate_food()){ // 检验输入正确
				$("#form2").ajaxSubmit({ // 提交表单
		            type: "post",
		            url: "doUpdateFood",
		            success: function (data) { // 处理后台返回的结果
						  var res = eval("("+ data +")");
					    	if (res.result == "error"){ // 出错
					    		$(".alert-danger").show("slow");
					    		if (res.help.name != ""){
					    			$("#f-helpName").text(res.help.name);
					    			return ;
					    		}
					    		if (res.help.type != ""){
					    			$("#f-helpType").text(res.help.type);
					    			return ;
					    		}
					    		if(res.help.taste != ""){
					    			$("#f-helpTaste").text(res.help.taste);
					    			return ;
					    		}
					    	}else if(res.result == "deny"){ // 未登录
					    		location.href = "shopLogin?from=myFoods";
					    	}else if (res.result == "success"){ // 执行成功
					    		$(".alert-success").show("slow");
					    	}
					  }
					});
			}
			else
				$(".alert-danger").show("slow");
		});
	});
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/userCenter.css">
<style>
.order-detail-head p{
        width:100px;
        text-align: center;
    }
    .order-detail-head h2{
        width:80px;
        text-align: center;
    }
    .order-detail-head h3{
        width:100px;
        text-align: center;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail-head h4{
        width: 150px;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail-head h5{
        width:200px;
        text-align: center;
    }
    .order-detail dl{
        float: left;
        width: 100px;
    }
    .order-detail h2{
        width: 80px;
    }
    .order-detail h3{
        width: 100px;
    }
    .order-detail h4{
        width: 150px;
    }
    .order-detail h5{
        width: 200px;
        line-height:20px;
    }
</style>

</head>
<body>
	<!-- 导航 -->
	<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
							</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	
	<!-- 主要内容 -->
	<div class="container-fluid">
        <div class="col-md-1"></div>
		<div class="col-md-2">
            <div class="title">商家中心</div>
				<ul class="left-menu">
					<li>
						<a href="myOrders"><span>我的订单</span></a>
					</li>
					<li class="cur">
	                	<a href="myFoods"><span>我的商品</span></a>
					</li>
					<li>
						<a href="#shopModal" data-toggle="modal"><span>店铺信息</span></a>
					</li>
					<li>
	                    <a href="#passwdModal" data-toggle="modal"><span>修改密码</span></a>
					</li>
				</ul>
		</div>
		<div class="col-md-8">
			<div class="s-margin">
				<input id="txtSearch" type="text" placeHolder="菜名">
				<span id="search" class="s-btn">商品搜索</span>
				<span id="add" class="btn-n">添加</span>
			</div>
			<input id="q" type="hidden">
			<input type="hidden" name="page" id="page" value="${page}">
			<input type="hidden" id="total" value="${total }">
			<input type="hidden" id="state" value="${sessionScope.shop.state }">
            <div class="order">
            <c:forEach items="${foods}" var="food">
                <div class="order-title">
                    <p>商品id:<span class="id">${food.id}</span></p>
                    <p>菜名：${food.name}</p>
                    <p>发布时间: ${food.releaseTime }</p>
                    <p>销量:${food.sale }</p>
                    <p>状态:
                    <input class="state" type="hidden" value="${food.state}">
						<span class="state">
							<c:if test="${food.state == 0}">已下架</c:if>
							<c:if test="${food.state == 1}">销售中</c:if>
						</span>
                    </p>
                    <p>操作:
						<c:if test="${food.state == 1}">		
						<a class="edit" href="javascript:;">编辑</a>
						<a class="close-f" href="javascript:;">下架</a>
						</c:if>
                    </p>
                </div>
                <div class="order-detail-head">
                    <p>图片</p>
                    <h2>单价</h2>
                    <h2>制作时间</h2>
                    <h3>类型</h3>
                    <h3>口味</h3>
                    <h4>用料</h4>
                    <h5>描述</h5>
                </div>
                <div class="order-detail">
                    <dl>
                        <dt>
                            <a href="food?id=${food.id}" target="_blank">
                            <img width="87" height="87" src="img/${food.img }">
                            </a>
                        </dt>
                    </dl>
                    <h2>${food.price}</h2>
                    <h2>${food.time}</h2>
                    <h3>${food.type}</h3>
                    <h3>${food.taste}</h3>
                    <h4>${food.ingredients}</h4>
                    <h5>${food.description }</h5>
                </div>
            </c:forEach>    
            </div>
			<div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                	<c:if test="${page <= 1 }">
                    <span class="btnpg disabled">上一页</span>
                    </c:if>
                    <c:if test="${page > 1 }">
                    <span class="btnpg prevPage">上一页</span>
                    </c:if>
                    <c:if test="${page == 1 }">
                    <span class="btnpg current">1</span>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <a class="btnpg" href="javascript:;">1</a>
                    </c:if>
                    <c:if test="${page - 3 > 1 }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${page - 2 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 2}</a>
                    </c:if>
                    <c:if test="${page - 1 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 1}</a>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <span class="btnpg current">${page }</span>
                    </c:if>
                    <c:if test="${page + 1 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 1}</a>
                    </c:if>
                    <c:if test="${page + 2 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 2}</a>
                    </c:if>
                    <c:if test="${page + 3 < pages }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${pages > page }">
                    <a class="btnpg" href="javascript:;">${pages}</a>
                    </c:if>
                    <c:if test="${page >= pages }">
                    <span class="btnpg disabled">下一页</span>
                    </c:if>
                    <c:if test="${page < pages }">
                    <a class="btnpg nextPage" href="javascript:;">下一页</a>
                    </c:if>
                    
                </span>
                </div>
            </div>
		</div>
	</div>
	
		<!-- 用户信息 -->
	<div class="modal fade" id="shopModal" tabindex="-1" role="dialog" aria-labelledby="shopLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="shopLabel">修改信息</h4>
	      </div>
	      <div class="modal-body">
	      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
	      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
	      
			<form action="" id="form1" onsubmit="return false;">
				<div class="form-group">
					<label>用户ID：</label>
					<input class="form-control" type="text" name="shop.id" readonly="readonly" value ="${shop.id}">
				</div>
				<div class="form-group">
					<label>用户名：</label>
					<input class="form-control" name="shop.name" type="text" readonly="readonly" placeHolder="username" value="${shop.name}">
				</div>
				<div class="form-group">
					<label>邮箱：</label>
					<input class="form-control" id="email" name="shop.email" type="text" placeHolder="如abc@abc.abc" value="${shop.email}">
		            <label class="help" id="helpEmail">${help.email}</label>
				</div>
				<div class="form-group">
					<label>联系电话/手机：</label>
					<input class="form-control" id="phone" name="shop.phone" type="text" placeHolder="如100-1000-1000" value="${shop.phone }">
					<label class="help" id="helpPhone">${help.phone}</label> 
				</div>
				<div class="form-group">
					<label>店铺名：</label>
					<input class="form-control" id="shopName" name="shop.shopName" type="text" placeHolder="30位字符" value="${shop.shopName}">
					<label class="help" id="helpShopName">${help.shopName}</label>
				</div>
				<div class="form-group">
					<label>真实姓名:</label>
					<input class="form-control" id="ownerName" name="shop.ownerName" type="text" placeHolder="" value="${shop.ownerName}">
					<label class="help" id="helpOwnerName"></label>
				</div>
				<div class="form-group">
					<label>身份证号:</label>
					<input class="form-control" id="idNumber" name="shop.idNumber" type="text" placeHolder="" value="${shop.idNumber}">
					<label class="help" id="helpIdNumber"></label>
				</div>
				<div class="form-group">
					<label>店铺地址：</label>
					<input class="form-control" id="address" name="shop.address" type="text" placeHolder="" value="${shop.address}">
					<label class="help" id="helpAddress"></label>
				</div>
				<div class="form-group">
					<label>邮政编码：</label>
					<input class="form-control" id="zip" name="shop.zip" type="text" value="${shop.zip}" placeHolder="6位数字">
					<label class="help" id="helpZip"></label>
				</div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
	        <button type="button" id="save" class="btn btn-primary">保存</button>
	      </div>
	    </div>
	  </div>
	</div>
	
		<!-- 修改密码 -->
	<div class="modal fade" id="passwdModal" tabindex="-1" role="dialog" aria-labelledby="passwdLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="passwdLabel">修改密码</h4>
	      </div>
	      <div class="modal-body">
	      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
	      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
	            <div class="form-group">
					<label>密码：</label>
					<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码不能为空">
					<label class="help" id="helpPasswd"></label>
				</div>
				<div class="form-group">
					<label>重新输入密码：</label>
					<input class="form-control" id="rePasswd" name="shop.passwd" type="password" placeHolder="password">
					<label class="help" id="helpRePasswd"></label>
				</div>
	      </div>
	            
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
	        <button type="button" id="savePasswd" class="btn btn-primary">保存</button>
	      </div>
	    </div>
	  </div>
	</div>
	
		<!-- 菜单信息 -->
	<div class="modal fade" id="foodModal" tabindex="-1" role="dialog" aria-labelledby="foodLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="foodLabel">商品信息</h4>
	      </div>
	      <div class="modal-body">
	      <div class="f-message"></div>
	      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
	      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
				<form id="form2" action="doUpdateFood" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>菜ID：</label>
					<input class="form-control" id="f-id" type="text" name="food.id" readonly="readonly">
				</div>
				<div class="form-group">
					<label>菜名：</label>
					<input class="form-control" id="f-name" name="food.name" type="text">
		            <label class="help" id="f-helpName">${help.name}</label>
				</div>
				<div class="form-group">
					<label>单价：</label>
					<input class="form-control" id="f-price" name="food.price" type="text" placeHolder="如8.5">
					<label class="help" id="f-helpPrice"></label>
				</div>
				<div class="form-group">
					<label>制作时间/分钟：</label>
					<input class="form-control" id="f-time" name="food.time" type="text" placeHolder="如20">
					<label class="help" id="f-helpTime"></label>
				</div>
				<div class="form-group">
					<label>描述：</label>
					<input class="form-control" id="f-des" name="food.description" type="text">
					<label class="help" id="f-helpdescription"></label>
				</div>
				<div id="f-taste" class="form-group">
					<label>口味：</label>
					<input type="checkbox" name="food.taste" checked="checked" value="普通">普通
					<input type="checkbox" name="food.taste" value="清淡">清淡
					<input type="checkbox" name="food.taste" value="辣">辣
					<input type="checkbox" name="food.taste" value="酸">酸
					<input type="checkbox" name="food.taste" value="甜">甜
					<label class="help" id="f-helpTaste"></label>
				</div>
				<div class="form-group">
					<label>类型：</label>
					<select id="f-type" name="food.type">
						<option value="快餐">快餐</option>
						<option value="小炒">小炒</option>
						<option value="油炸">油炸</option>
						<option value="清蒸">清蒸</option>
						<option value="汤">汤</option>
						<option value="甜品">甜品</option>
						<option value="炖">炖</option>
						<option value="烤">烤</option>
						<option value="其他">其他</option>				
					</select>
					<label class="help" id="f-helpType"></label>
				</div>
				<div class="form-group">
					<label>用料：</label>
					<input class="form-control" id="f-source" name="food.ingredients" type="text" placeHolder="多种用料用分号(;)隔开">
					<label class="help" id="f-helpSource"></label>
				</div>
				<div class="form-group">
					<label>照片：</label>
					<input id="f-setImg" name="image" type="file" onchange="checkImg(this)" >
				</div>
				<div class="form-group">
					<label id="f-helpImg"></label>
					<img id="f-img" width="100px" height="50px">
				</div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
	        <button type="button" id="saveFood" class="btn btn-primary">保存</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	      <!-- footer -->
	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>