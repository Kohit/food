<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<meta charset="gbk">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改店铺信息</title>
<script src="js/jquery.min.js"></script>
<script src="js/validate_shop.js"></script>
<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>修改店铺信息</h1>
            </div>
        </div>
    </div>
	<hr />
	<div class="container">
        <div class="row" >
            <div class="col-md-7" >
            </div>
        </div>
		<div class="well pull-right col-md-4">
		<form action="doChShopInfo" method="post">
			<div class="form-group">
				<label>用户ID：</label>
				<label>${id}</label>
			</div>
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" name="shop.name" type="text" readonly="readonly" placeHolder="username" value="${shop.name}">
			</div>
			<div class="form-group">
				<label>邮箱：</label>
				<input class="form-control" id="email" name="shop.email" type="text" placeHolder="如abc@abc.abc" value="${shop.email}">
	            <label class="help" id="helpEmail">${help.email}</label>
			</div>
			<div class="form-group">
				<label>联系电话/手机：</label>
				<input class="form-control" id="phone" name="shop.phone" type="number" placeHolder="如100-1000-1000" value="${shop.phone }">
				<label class="help" id="helpPhone">${help.phone}</label> 
			</div>
			<div class="form-group">
				<label>店铺名：</label>
				<input class="form-control" id="shopName" name="shop.shopName" type="text" placeHolder="30位字符" value="${shop.shopName}">
				<label class="help" id="helpShopName">${help.shopName}</label>
			</div>
			<div class="form-group">
				<label>真实姓名:</label>
				<input class="form-control" id="ownerName" name="shop.ownerName" type="text" placeHolder="" value="${shop.ownerName}">
				<label class="help" id="helpOwnerName"></label>
			</div>
			<div class="form-group">
				<label>身份证号:</label>
				<input class="form-control" id="idNumber" name="shop.idNumber" type="number" placeHolder="" value="${shop.idNumber}">
				<label class="help" id="helpIdNumber"></label>
			</div>
			<div class="form-group">
				<label>店铺地址：</label>
				<input class="form-control" id="address" name="shop.address" type="text" placeHolder="" value="${shop.address}">
				<label class="help" id="helpAddress"></label>
			</div>
			<div class="form-group">
				<label>邮政编码：</label>
				<input class="form-control" id="zip" name="shop.zip" type="text" value="${shop.zip}" placeHolder="6位数字">
				<label class="help" id="helpZip"></label>
			</div>
			<div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码为空表示不修改">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="shop.passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
			<button type="submit" class="btn btn-primary active">修改</button>
		</form>
		</div>
	</div>

<!--  
<c:set var="user" value="${sessionScope.shop}"></c:set>
	<form action="doChShopInfo" method="post">
		<div>
			<label>店铺ID：</label>
			<input type="text" name="shop.id" readonly="readonly" value ="${shop.id}">
		</div>
		<div>
			<label>用户名：</label>
			<input name="shop.name" type="text" readonly="readonly" placeHolder="username" value="${shop.name}">
		</div>
		<div>
			<label>邮箱：</label>
			<input id="email" name="shop.email" type="text" placeHolder="如abc@abc.abc" value="${shop.email}">
            <label class="help" id="helpEmail">${help.email}</label>
		</div>
		<div>
			<label>联系电话/手机：</label>
			<input id="phone" name="shop.phone" type="text" placeHolder="如100-1000-1000" value="${shop.phone }">
			<label class="help" id="helpPhone">${help.phone}</label> 
		</div>
		<div>
			<label>店铺名：</label>
			<input id="shopName" name="shop.shopName" type="text" placeHolder="30位字符" value="${shop.shopName}">
			<label class="help" id="helpShopName">${help.shopName}</label>
		</div>
		<div>
			<label>真实姓名:</label>
			<input id="ownerName" name="shop.ownerName" type="text" placeHolder="" value="${shop.ownerName}">
			<label class="help" id="helpOwnerName"></label>
		</div>
		<div>
			<label>身份证号:</label>
			<input id="idNumber" name="shop.idNumber" type="text" placeHolder="" value="${shop.idNumber}">
			<label class="help" id="helpIdNumber"></label>
		</div>
		<div>
			<label>店铺地址：</label>
			<input id="address" name="shop.address" type="text" placeHolder="" value="${shop.address}">
			<label class="help" id="helpAddress"></label>
		</div>
		<div>
			<label>邮政编码：</label>
			<input id="zip" name="shop.zip" type="text" value="${shop.zip}" placeHolder="6位数字">
			<label class="help" id="helpZip"></label>
		</div>
		<div>
			<label>密码：</label>
			<input id="passwd" name="supasswd" type="password" placeHolder="密码为空表示不修改">
			<label class="help" id="helpPasswd"></label>
		</div>
		<div>
			<label>重新输入密码：</label>
			<input id="rePasswd" name="shop.passwd" type="password" placeHolder="password">
			<label class="help" id="helpRePasswd"></label>
		</div>
		<button type="submit">注册</button>
	</form>
	-->
</body>
</html>