<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>购物车</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("button#btnBuy").click(function(){
			$.post("check",{}, function(data){
				var u = eval("("+ data +")");
				if (u.res == "login"){
					alert("尚未登录");
					location.href="login?from=cart";
				}else if(u.res == "empty")
					alert("购物车为空");
				else if(u.res = "success"){
					location.href="checkout";
				}
			});
			
		});
		$("button.delete").click(function(){
			var id = $(this).siblings("input").serialize();
			$.post("rmBuy", id);
			var num = parseInt($(this).parents("tr").find("span.total").text());
			var to = parseInt($("#total").text());
			$("#total").text(to - num);
			$(this).parents("tr").remove();
		});
		$("button.min").click(function(){
			var count = $(this).siblings("span");
			if (parseInt(count.text()) <= 1) return;
			count.text(parseInt(count.text()) - 1);
			var id = $(this).siblings("input").serialize();
			$.post("minNum", id);
			var num = $(this).parents("tr").find("span.total");
			var cost = parseInt($(this).parents("tr").find("span.price").text());
			num.text(parseInt(count.text()) * cost);
			var to = parseInt($("#total").text());
			$("#total").text(to - cost);
		});
		$("button.add").click(function(){
			var count = $(this).siblings("span");
			if (parseInt(count.text()) >= 10) return; // 最多10个
			count.text(parseInt(count.text()) + 1);	
			var id = $(this).siblings("input").serialize();
			$.post("addNum", id);
			var num = $(this).parents("tr").find("span.total");
			var cost = parseInt($(this).parents("tr").find("span.price").text());
			num.text(parseInt(count.text()) * cost);
			var to = parseInt($("#total").text());
			$("#total").text(to + cost);
		});
	});
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<style type="text/css">
	th{
	text-align:center;
	}
	td.cen{
	line-height:5;
	text-align:center;
	}
	.last{
	width:100%;
	background-color:#e1dede;
	text-align:right;
	}
	.lasttotal{
	margin-right:30px;
	font-size:20px;
	color:#ff3e00;
	font-weight:bold;
	}
	.lastbutton{
	height:50px;
	width:100px;
	border:none;
	font-size:20px;
	background:#ff3e00;
	color:white;
	}
	.blr{
	height:20px;
	width:20px;
	border:none;
	background:#e1dede;
	color:black;
	}
	.bdele{
	background:white;
	color:black;
	border:none;
	}
</style>
</head>
<body style="background-color:white">
	<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div style="margin:0 auto;width:80%;">
	<div style="text-align:left;font-size:50px">&nbsp;购物车</div>
		<c:choose>
			<c:when test="${!empty(sessionScope.cart)}">
				<c:set value="0" var="total"></c:set>
				
				<table class="table table-bordered">
					<tr>
						<th>商品信息</th><th>单价</th><th>数量</th><th>金额(元)</th><th>操作</th>
					</tr>
				<c:forEach items="${sessionScope.cart}" var="item">
					<tr>
						<td>
							<div>
								<a href="food?id=${item.food.id }" target="_blank">
								<img src="img/${item.food.img}" style="width:100px">
								${item.food.name }
								</a>
							</div>
						</td>
						<td class="cen">
							<span class="price">${item.food.price}</span>
						</td>
						<td class="cen">
							<input type="hidden" name="id" value="${item.food.id}">
							<button class="min blr" >-</button>
							<span>${item.count }</span>
							<button class="add blr">+</button>
						</td>
						<td class="cen">
							<span class="total">${item.food.price * item.count }</span>
						</td>
						<c:set value="${total + item.food.price * item.count}" var="total"></c:set>
						<td class="cen">
							<div>
								<input type="hidden" name="id" value="${item.food.id}">
								<button class="delete bdele">删除</button>
							</div>
						</td>
					</tr>
				</c:forEach>
				</table>
				<div class="last">
				合计：<span class="lasttotal" id="total">${total}</span>
					<button id="btnBuy" class="lastbutton">结算</button>
				</div>
			</c:when>
			<c:otherwise>
				购物车为空
			</c:otherwise>
		</c:choose>
	</div>
	  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>