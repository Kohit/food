<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>信息统计</title>
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body style="background-color:white">
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车<span class="cartSize">(${fn:length(sessionScope.cart)})</span></a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div style="margin:0 auto;width:80%;">
		<h1>统计</h1>
		<a style="font-size:25px;"></a>
		<font size="4">菜销量Top10</font>
		<table class="table table-bordered">    
			<tr>
				<th>菜名</th><th>销量</th><th>评价</th><th>价格</th><th>类型</th><th>口味</th><th>店铺</th>
			</tr>
			<c:forEach items="${topSaleFoods}" var="food">
			<tr>
				<td>
					<div>
						<a href="food?id=${food.id}">
				<!-- <img src="img/${food.img}" width="100"> -->			
							${food.name }
						</a>
					</div>
				</td>
				<td>${food.sale}</td>
				<td>${food.score }</td>
				<td>${food.price }</td>
				<td>${food.type }</td>
				<td>${food.taste }</td>
				<td><a href="shop?id=${food.shop.id }">${food.shop.shopName }</a></td>
			</tr>
			</c:forEach>
		</table>
		<div style="font-size:15px">
			<p>最受欢迎类型：${mostType }</p>
			<p>最受欢迎口味：${mostTaste }</p>
		</div>
		<br/>
		<font size=4>店铺销售Top10</font>
		<table class="table table-bordered">
			<tr><th>店铺名</th><th>联系电话</th><th>地址</th></tr>
			<c:forEach items="${topShops}" var="shop">
			<tr>
				<td><a href="shop?id=${shop.id }">${shop.shopName }</a></td>
				<td>${shop.phone }</td>
				<td>${shop.address }</td>
			</tr>
			</c:forEach>
		</table>
		<br/>
		
		<font size="4">菜评价Top10</font>
		<table class="table table-bordered">
			<tr>
				<th>菜名</th><th>销量</th><th>评价</th><th>价格</th><th>类型</th><th>口味</th><th>店铺</th>
			</tr>
			<c:forEach items="${topScoreFoods}" var="food">
			<tr>
				<td>
					<div>
						<a href="food?id=${food.id}">
						<!-- <img src="img/${food.img}" width="100"> -->	
							${food.name }
						</a>
					</div>
				</td>
				<td>${food.sale}</td>
				<td>${food.score }</td>
				<td>${food.price }</td>
				<td>${food.type }</td>
				<td>${food.taste }</td>
				<td><a href="shop?id=${food.shop.id }">${food.shop.shopName }</a></td>
			</tr>
			</c:forEach>
		</table>
	</div>
	<div style="margin:0 auto;width:80%;font-size:15px">
		<c:if test="${!empty(sessionScope.user.id)}" >
			<h1>个人统计</h1>
			<p>我的购买数：${myStat.buy}</p>
			<p>我最喜欢的类型：${myStat.type }</p>
			<p>我最喜欢的口味：${myStat.taste }</p>
			<p>我最喜欢吃：${myStat.food }</p>
			<p>我总共花了：￥${myStat.price}</p>
		</c:if>
	</div>
	  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>