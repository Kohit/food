<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的收藏</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<style>
	body{
		background:#fff;
	}
		    a.food-name{
	    font-size:15px;
	    font-weight:bold;
	}
		a.food-shop{
		float:right;
		color:rgba(254, 78, 12, 0.62);
		}
	    p.food-price{
	    font-size:15px;
	    color:red;
	    }
	    div.food-info{
	    padding-top:5px;
	    }
	    #food-list{
	    	width:100%;
	    	border:none;
	    }
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div style="width:100%;height:50px;background-color:#ff3e00;margin:0px">
        <div style="height:25px;font-size:30px;color:white;margin-left:20px;line-height:50px">收藏夹</div>
    </div>
	<div id="food-list">
	<ul class="list-wrap">
            	<c:forEach items="${collections}" var="item" varStatus="i">
				<c:if test="${i.count % 5 == 0}">
					<li class="list aver new-line">
				</c:if>
				<c:if test="${i.count % 5 != 0}">
					<li class="list aver">
				</c:if>
                
                    <div class="img">
                        <a href="food?id=${item.food.id}" target="_blank">
                            <img title="" src="img/${item.food.img }" />
                        </a>
                    </div>
                      <div class="food-info">
                        <a class="food-name" href="food?id=${item.food.id}" target="_blank">${item.food.name}</a>       
                            <a class="food-shop" href="shop?id=${item.food.shop.id}">${item.food.shop.shopName }</a>
                    </div>
                    <div>
                        <p class="food-rank">
                            <span>评价:${item.food.score}</span>
                            &nbsp;&nbsp;&nbsp;
                            <span>销量:${item.food.sale}</span>
                        </p>
                        <p class="food-price">￥${item.food.price }</p>  
                    </div>

                </li>
            </c:forEach>
            </ul>
	</div>
	
</body>
</html>