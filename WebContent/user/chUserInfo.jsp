<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改用户信息</title>
<script src="js/jquery.min.js"></script>
<script src="js/validate_user.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<c:set var="user" value="${sessionScope.user}"></c:set>
	<div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>修改用户信息</h1>
            </div>
        </div>
    </div>
	<hr />
	<div class="container">
        <div class="row" >
            <div class="col-md-7" >
            </div>
        </div>
		<div class="well pull-right col-md-4">
		<form action="doChUserInfo" method="post">
			<div class="form-group">
				<label>用户ID：</label>
				<label>${user.id}</label>
			</div>
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" name="user.name" type="text" readonly="readonly" value="${user.name}">
			</div>
			<div class="form-group">
				<label>邮箱：</label>
				<input class="form-control" id="email" name="user.email" type="email" placeHolder="如abc@abc.abc" value="${user.email}">
	            <label class="help" id="helpEmail">${help.email}</label>
			</div>
			<div class="form-group">
				<label>收件人名：</label>
				<input class="form-control" id="realName" name="user.realName" type="text" value="${user.realName}">
				<label class="help" id="helpRealName"></label>
			</div>
			<div class="form-group">
				<label>联系电话/手机：</label>
				<input class="form-control" id="phone" name="user.phone" type="number" value="${user.phone}">
				<label class="help" id="helpPhone">${help.phone}</label>
			</div>
			<div class="form-group">
				<label>地址：</label>
				<input class="form-control" id="address" name="user.address" type="text" value="${user.address}">
				<label class="help" id="helpAddress"></label>
			</div>
			<div class="form-group">
				<label>邮政编码：</label>
				<input class="form-control" id="zip" name="user.zip" type="text" value="${user.zip}" placeHolder="6位数字">
				<label class="help" id="helpZip"></label>
			</div>
			<div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码为空表示不修改">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="user.passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
			<button type="submit" class="btn btn-primary active">保存</button>
		</form>
	  </div>
	</div>

<!--  
	<c:set var="user" value="${sessionScope.user}"></c:set>
	<form action="doChUserInfo" method="post">
		<div>
			<label>用户ID：</label>
			<input type="text" name="user.id" readonly="readonly" value ="${user.id}">
		</div>
		<div>
			<label>用户名：</label>
			<input name="user.name" type="text" readonly="readonly" value="${user.name}">
		</div>
		<div>
			<label>邮箱：</label>
			<input id="email" name="user.email" type="text" placeHolder="如abc@abc.abc" value="${user.email}">
            <label class="help" id="helpEmail">${help.email}</label>
		</div>
		<div>
			<label>收件人名：</label>
			<input id="realName" name="user.realName" type="text" value="${user.realName}">
			<label class="help" id="helpRealName"></label>
		</div>
		<div>
			<label>联系电话/手机：</label>
			<input id="phone" name="user.phone" type="text" value="${user.phone}">
			<label class="help" id="helpPhone">${help.phone}</label>
		</div>
		<div>
			<label>地址：</label>
			<input id="address" name="user.address" type="text" value="${user.address}">
			<label class="help" id="helpAddress"></label>
		</div>
		<div>
			<label>邮政编码：</label>
			<input id="zip" name="user.zip" type="text" value="${user.zip}" placeHolder="6位数字">
			<label class="help" id="helpZip"></label>
		</div>
		<div>
			<label>密码：</label>
			<input id="passwd" name="supasswd" type="password" placeHolder="密码为空表示不修改">
			<label class="help" id="helpPasswd"></label>
		</div>
		<div>
			<label>重新输入密码：</label>
			<input id="rePasswd" name="user.passwd" type="password" placeHolder="password">
			<label class="help" id="helpRePasswd"></label>
		</div>
		<button type="submit">保存</button>
	</form>
	-->
</body>
</html>