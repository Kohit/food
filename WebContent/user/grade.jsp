<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>评价</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("button").click(function(){
			$("form").submit();
		});
	});
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<style>
 .pl{
	height:35px;
	width:70px;
	border:none;
	font-size:20px;
	background:#ff3e00;
	color:white;
	}
	.text{
	width:350px;
	height:100px;
	}
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div style="width:100%;height:50px"></div>
	<div style="background:white;width:90%;height:400px;margin:0 auto;text-align:center">
	<div style="width:100;height:60px"></div>
	<div class="container">   
            <div class="col-md-4">
            	<a href="food?id=${food.id}">
				<span class="logo">
					<img src="img/${food.img }" style="width:250px;height:180px">
				</span><br/>
				<span style="font-size:20px">${food.name}</span><br/>
				</a>
            </div>

        <div class="col-md-7" style="margin-left:0px"> 
		<form action="doGrade" method="post">
				<input name="id" type="hidden" value="${food.id}">
				<div class="form-group">	
					<textarea style="width:550px;height:150px" name="comment" class="text"  placeholder="亲，你对本次购物满意么？写点什么与大家分享吧。"></textarea>
				</div>
				
				<div class="form-group">
				<button type="submit" onclick="return validate()" class="pl">发表</button>
					请打分：
					<select name="grade">
						<option value="5">5</option>
						<option value="4">4</option>
						<option value="3">3</option>
						<option value="2">2</option>
						<option value="1">1</option>
					</select>
				</div>
				
			
		</form>
	</div>
	</div>
   </div>
  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>