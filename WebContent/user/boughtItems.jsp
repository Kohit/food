<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的订单</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/validate_user.js"></script>
<script src="js/setPage.js"></script>
<script>
	$(document).ready(function(){

		$("#search").click(function(){
			$("#q").val($("#txtSearch").val());
			location.href="boughtItems?q=" + encodeURIComponent($("#q").val());
		});
		$(".trade").click(function(){
			var oid = $(this).parents(".order-title").find("span.oid").text();
			var state = parseInt($(this).parents(".order-title").find("input.state").val());
			if (state == 1){
				if(!confirm("是否确认签收?")) return ;
				$.post("confirmBuy", {id:oid});
				$(this).parents(".order-title").find("input.state").val("2");
				$(this).parents(".order-title").find("span.state").text("交易完成");
				$(this).remove();
			}else if (state == 0){
				if(!confirm("是否确认关闭?")) return ;
				$.post("returnItem", {id:oid});
				$(this).parents(".order-title").find("input.state").val("3");
				$(this).parents(".order-title").find("span.state").text("交易已关闭");
				$(this).remove();

			}
		});
		$(".order-title").click(function(){
			$(this).nextUntil(".order-title").toggle("slow");
		});
		$("a.btnpg").click(function(){
		    var cur = parseInt($("#page").val());
		    var last = parseInt($(".totalPage").text());
		   if ($(this).hasClass("prevPage"))
		       cur--;
		   else if ($(this).hasClass("nextPage"))
		       cur++;
		   else
		       cur = parseInt($(this).text());
		   $("#page").val(cur);
		   setPage(cur, last);
		   
		   location.href="boughtItems?q=" + encodeURIComponent($("#q").val()) + "&page=" + $("#page").val();
		
		});
		$("#save").click(function(){
			$(".alert").hide("slow");
			if (validate_user()){
				var x=$("form").serializeArray();
				$.post("doChUserInfo",x,function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){ // 执行错误
			    		$(".alert-danger").show("slow");
			    		if (res.help.email != ""){ // 邮箱出错
			    			$("#helpEmail").text(res.help.email);
			    			return ;
			    		}
			    		if (res.help.phone != ""){ // 手机号出错
			    			$("#helpPhone").text(res.help.phone);
			    			return ;
			    		}
			    	}else if(res.result == "deny"){ // 未登录
			    		location.href = "login?from=boughtItems";
			    	}else if (res.result == "success"){ // 执行成功
			    		$(".alert-success").show("slow");
			    	}
				}); 
			}else
				$(".alert-danger").show("slow");
		});
		$("#savePasswd").click(function(){
			$(".alert").hide("slow");
			if (validate_passwd()){
				$.post("doChUserPasswd",$("#rePasswd").serialize(),function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    	}else if(res.result == "deny"){
			    		location.href = "login?from=boughtItems";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
				}); 
			}else
				$(".alert-danger").show("slow");
		});
	});

</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/userCenter.css">
</head>
<body>
<!-- 导航 -->
	<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<!-- 主要内容 -->
	<div class="container-fluid">
        <div class="col-md-1"></div>
		<div class="col-md-2">
            <div class="title">用户中心</div>
				<ul class="left-menu">
					<li class="cur">
						<a href="javascript:;"><span>我的订单</span></a>
					</li>
					<li>
						<a href="#myModal" data-toggle="modal"><span>个人信息</span></a>
					</li>
	                <li>
	                	<a href="myCollections" target="_blank"><span>我的收藏</span></a>
					</li>
					<li>
	                    <a href="#passwdModal" data-toggle="modal"><span>修改密码</span></a>
					</li>
				</ul>
		</div>
		<div class="col-md-7">
			<div class="s-margin">
				<input id="txtSearch" type="text" placeHolder="订单号或菜名">
				<span id="search" class="s-btn">订单搜索</span>
			</div>
			<form>
			<input id="q" type="hidden">
			<input type="hidden" name="page" id="page" value="${page}">
			<input type="hidden" id="total" value="${total }">
			</form>
			<!-- 详情 -->
            <div class="order">
            <c:forEach items="${orders}" var="order">
                <div class="order-title">
                    <p>订单号:<span class="oid">${order.id}</span></p>
                    <p>店铺
                        <a href="shop?id=${order.shop.id }" target="_blank">${order.shop.shopName }</a>
                    </p>
                    <p>下单时间: ${order.time }</p>
                    <p>状态:
                    <input class="state" type="hidden" value="${order.state}">
						<span class="state">
							<c:if test="${order.state == 0}">尚未配送</c:if>
							<c:if test="${order.state == 1}">未签收</c:if>
							<c:if test="${order.state == 2}">交易完成</c:if>
							<c:if test="${order.state == 3}">交易已关闭</c:if>
						</span>
                    </p>
                    <p>操作:
						<c:if test="${order.state == 1}">		
						<a class="trade" href="javascript:;">确认收货</a>
						</c:if>
						<c:if test="${order.state == 0}">
						<a class="trade" href="javascript:;">取消交易</a>
						</c:if>
                    </p>
                </div>
                <div class="order-detail-head">
                    <p>商品信息</p>
                    <h2>单价</h2>
                    <h3>数量</h3>
                    <h4>实付款</h4>
                    <h5>操作</h5>
                </div>
                <c:forEach items="${order.orderDetail}" var="item">
                <div class="order-detail">
                    <dl>
                        <dt>
                            <a href="food?id=${item.food.id}" target="_blank">
                            <img width="87" height="87" src="img/${item.food.img }">
                            </a>
                        </dt>
                        <dd>
                            <a href="food?id=${item.food.id }" target="_blank">${item.food.name }</a>
                        </dd>
                    </dl>
                    <h2>${item.price}</h2>
                    <h3>${item.amount }</h3>
                    <h4>${item.price * item.amount }</h4>
                    <h5>
                    <c:if test="${order.state == 2 }">
						<a href="grade?id=${item.food.id}">评价</a>
					</c:if>
                    </h5>
                </div>
                </c:forEach>
            </c:forEach>    
            </div>
        <!-- 分页 -->    
		<div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                	<c:if test="${page <= 1 }">
                    <span class="btnpg disabled">上一页</span>
                    </c:if>
                    <c:if test="${page > 1 }">
                    <span class="btnpg prevPage">上一页</span>
                    </c:if>
                    <c:if test="${page == 1 }">
                    <span class="btnpg current">1</span>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <a class="btnpg" href="javascript:;">1</a>
                    </c:if>
                    <c:if test="${page - 3 > 1 }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${page - 2 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 2}</a>
                    </c:if>
                    <c:if test="${page - 1 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 1}</a>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <span class="btnpg current">${page }</span>
                    </c:if>
                    <c:if test="${page + 1 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 1}</a>
                    </c:if>
                    <c:if test="${page + 2 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 2}</a>
                    </c:if>
                    <c:if test="${page + 3 < pages }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${pages > page }">
                    <a class="btnpg" href="javascript:;">${pages}</a>
                    </c:if>
                    <c:if test="${page >= pages }">
                    <span class="btnpg disabled">下一页</span>
                    </c:if>
                    <c:if test="${page < pages }">
                    <a class="btnpg nextPage" href="javascript:;">下一页</a>
                    </c:if>
                    
                </span>
                </div>
            </div>
		</div>
	</div>

        <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">修改信息</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
        <form action="" onsubmit="return false;">
			<div class="form-group">
				<label>用户ID：</label>
				<input class="form-control" type="text" name="user.id" readonly="readonly" value ="${user.id}">
			</div>
			<div class="form-group">
				<label>用户名：</label>
				<input class="form-control" name="user.name" type="text" readonly="readonly" value="${user.name}">
			</div>
			<div class="form-group">
				<label>邮箱：</label>
				<input class="form-control" id="email" name="user.email" type="text" placeHolder="如abc@abc.abc" value="${user.email}">
	            <label class="help" id="helpEmail">${help.email}</label>
			</div>
			<div class="form-group">
				<label>收件人名：</label>
				<input class="form-control" id="realName" name="user.realName" type="text" value="${user.realName}">
				<label class="help" id="helpRealName"></label>
			</div>
			<div class="form-group">
				<label>联系电话/手机：</label>
				<input class="form-control" id="phone" name="user.phone" type="text" value="${user.phone}">
				<label class="help" id="helpPhone">${help.phone}</label>
			</div>
			<div class="form-group">
				<label>地址：</label>
				<input class="form-control" id="address" name="user.address" type="text" value="${user.address}">
				<label class="help" id="helpAddress"></label>
			</div>
			<div class="form-group">
				<label>邮政编码：</label>
				<input class="form-control" id="zip" name="user.zip" type="text" value="${user.zip}" placeHolder="6位数字">
				<label class="help" id="helpZip"></label>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="save" class="btn btn-primary">保存</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="passwdModal" tabindex="-1" role="dialog" aria-labelledby="passwdLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="passwdLabel">修改密码</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
      
            <div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码为空表示不修改">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="user.passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="savePasswd" class="btn btn-primary">保存</button>
      </div>
    </div>
  </div>
</div>

<!-- footer -->
	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>