<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>评论管理</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/validate_user.js"></script>
<script src="js/setPage.js"></script>
<script>
function postdata(data){
	$(".panel-title").text("");
	$("#comment").empty();
	$("#message").text("加载中...");
	$.post("fetchComment", $("form").serializeArray(), function(data){
		var res = eval("("+ data +")");
		if (res.result == "deny"){
			location.href="adLogin?from=adComment";
		}else if (res.result == "not found"){
			$("#message").text("菜id没有找到");
		}else if(res.result == "success"){
			setPage(res.page, res.pages);
			$("#message").text("");
			if (res.food.comments.length < 1)
				$("#message").text("尚无评论");
			$(".panel-title").html("<a href='food?id=" + res.food.id + "' target='_blank'>" + res.food.name + "</a>");
	    	$.each(res.food.comments, function(i, com){
	    		
	    		var str = "<li class='comment-list'><div class='comment-title'>";
	    		str += com.user.name + "<span class='badge'>" + com.grade + "</span>";
	    		str += "<a class='delete' href='javascript:;'>删除</a>";
	    		str += "<input type='hidden' value='" + com.id + "'></div>";
	    		str += "<div class='comment-content'>" + com.content + "</div>";
	    		str += "<div class='comment-time'>" + com.time + "</div></li>";
	    		$("#comment").append(str);
			});
		}
	});
}
	$(document).ready(function(){
		$("#search").click(function(){
			$("#q").val($("#txtSearch").val());
			postdata();
		});
		$("#savePasswd").click(function(){
			$(".alert.alert-danger").hide("slow");
			if (validate_passwd()){
				$.post("doChAdPasswd",$("#rePasswd").serialize(),function(data){
			    	var res = eval("("+ data +")");
			    	if (res.result == "error"){
			    		$(".alert-danger").show("slow");
			    	}else if(res.result == "deny"){
			    		location.href = "adLogin?from=adComment";
			    	}else if (res.result == "success"){
			    		$(".alert-success").show("slow");
			    	}
				}); 
			}else
				$(".alert-danger").show("slow");
		});
	});
	$(document).on("click", "a.delete", function(){
		if (!confirm("该操作将设置用户评论内容为空，是否继续?")) return ;
		var id = $(this).siblings("input").val();
		$.post("deleteComment", {id:id});
		$(this).parents(".comment-list").remove();
	});
	$(document).on("click", "a.btnpg", function(){
	    var cur = parseInt($("#page").val());
	    var last = parseInt($(".totalPage").text());
	   if ($(this).hasClass("prevPage"))
	       cur--;
	   else if ($(this).hasClass("nextPage"))
	       cur++;
	   else
	       cur = parseInt($(this).text());
	   $("#page").val(cur);
	   setPage(cur, last);
	    postdata();
	});
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/userCenter.css">
<style>
                    .comment-board{
                         width: 100%;
                        height: auto;
                        padding-top: 24px;
                        position: relative;
                        padding-bottom: 14px;
                        overflow:hidden;
                    }
                    .comment-list{
                        border-bottom: 1px solid #e5e9ef;
                        padding-top: 20px;
                        padding-bottom: 10px;
                    }
                    .comment-title{
                        color:#222;
                        line-height: 18px;
                        padding-bottom: 8px;
                         font-size: 14px;
                    }
                    .comment-content{
                        line-height: 20px;
                        font-size: 14px;
                        overflow: hidden;
                        word-wrap: break-word;
                        padding-bottom: 8px;
                    }
                    .comment-time{
                        color:#aaa;
                        line-height: 14px;
                    }
                    body{
                    	background: #fff;
                    }
                    a.delete{
                    	margin-left:20px;
                    }
</style>
</head>
<body>
	
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myOrders" target="_blank">商家中心</a>
        </div>
	</div>
	<div class="container-fluid">
        <div class="col-md-1"></div>
		<div class="col-md-2">
            <div class="title">管理中心</div>
				<ul class="left-menu">
					<li class="cur">
						<a href="adComment"><span>评论管理</span></a>
					</li>
					<li>
						<a href="adShop"><span>店铺审核</span></a>
					</li>
					<li>
	                    <a href="#passwdModal" data-toggle="modal"><span>修改密码</span></a>
					</li>
				</ul>
		</div>
		<div class="col-md-7">
			<div class="s-margin">
				<input id="txtSearch" type="text" placeHolder="菜id">
				<span id="search" class="s-btn">搜索</span>
			</div>
			<form>
			<input type="hidden" name="id" id="q">
			<input type="hidden" name="page" id="page">
			<input type="hidden" id="total">
			</form>
            <div class="comment-board">
        		<div class="panel panel-default">
            		<div class="panel-heading">
                		<h3 class="panel-title"></h3>
            		</div>
            		<div class="panel-body">
            		<span id="message">
            		<c:if test="${fn:length(food.comments) == 0}">
            			尚无评论，请先搜索指定商品
            		</c:if>
            		</span>
                		<ul id="comment">
                			
                		</ul>
            	</div>
        	</div>
        </div>
		<div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                </span>
                </div>
            </div>
		</div>
	</div>
	
	<!-- Modal -->
<div class="modal fade" id="passwdModal" tabindex="-1" role="dialog" aria-labelledby="passwdLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="passwdLabel">修改密码</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-success" role="alert" style="display:none">修改成功</div>
      <div class="alert alert-danger" role="alert" style="display:none">修改失败，请检查输入</div>
            <div class="form-group">
				<label>密码：</label>
				<input class="form-control" id="passwd" name="supasswd" type="password" placeHolder="密码不能为空">
				<label class="help" id="helpPasswd"></label>
			</div>
			<div class="form-group">
				<label>重新输入密码：</label>
				<input class="form-control" id="rePasswd" name="passwd" type="password" placeHolder="password">
				<label class="help" id="helpRePasswd"></label>
			</div>
      </div>
            
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" id="savePasswd" class="btn btn-primary">保存</button>
      </div>
    </div>
  </div>
</div>
	
	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
	
</body>
</html>