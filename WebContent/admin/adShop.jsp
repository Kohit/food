<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>店铺审核</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("#search").click(function(){
			$("#q").val($("#txtSearch").val());
			location.href="adShop?q=" + encodeURIComponent($("#q").val());
		})
		$(".decide").click(function(){
			var id = $(this).parents(".order-detail").find("span.id").text();
			var state = $(this).parents(".order-detail").find("input.state").val();
			if (state == 1){
				if(!confirm("该操作将令商家无法发布商品，是否确认?")) return ;
				$.post("doDeny", {id:id});
				$(this).parents(".order-detail").find("input.state").val("0");
				$(this).parents(".order-detail").find("span.state").text("未批准");
				$(this).text("批准");
			}else if (state == 0){
				if(!confirm("商家将可以发布商品，是否确认?")) return ;
				$.post("doPermit", {id:id});
				$(this).parents(".order-detail").find("input.state").val("1");
				$(this).parents(".order-detail").find("span.state").text("已批准");
				$(this).text("不批准");
			}
		});
		$("a.btnpg").click(function(){
		    var cur = parseInt($("#page").val());
		    var last = parseInt($(".totalPage").text());
		   if ($(this).hasClass("prevPage"))
		       cur--;
		   else if ($(this).hasClass("nextPage"))
		       cur++;
		   else
		       cur = parseInt($(this).text());
		   $("#page").val(cur);
		   setPage(cur, last);
		   
		   location.href="adShop?q=" + encodeURIComponent($("#q").val()) + "&page=" + $("#page").val();
		
		});

	});
	function setPage(cur, last){
		$("#paging-food").empty();
		   var str = "";
		   if (cur <= 1)
		       str = "<span class='btnpg disabled'>上一页</span>";
		   else
		       str = "<a class='btnpg prevPage' href='javascript:;'>上一页</a>"
		   if (cur == 1)
		       str += "<span class='btnpg current'>1</span>";
		   else
		       str += "<a class='btnpg' href='javascript:;'>1</a>";
		   if (cur - 3 > 1)
		       str += "<span class='btnpg abb'>...</span>";
		   if (cur - 2 > 1)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur - 2) + "</a>";
		   if (cur - 1 > 1)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur - 1) + "</a>";
		   if (cur != 1)
		       str += "<span class='btnpg current'>" + cur + "</span>";
		   if (cur + 1 < last)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur + 1) + "</a>";
		   if (cur + 2 < last)
		       str += "<a class='btnpg' href='javascript:;'>" + (cur + 2) + "</a>";
		   if (cur + 3 < last)
		       str += "<span class='btnpg abb'>...</span>";
		   if (last > cur)
		       str += "<a class='btnpg' href='javascript:;'>" + last + "</a>";
		   if (cur >= last)
		       str += "<span class='btnpg disabled'>下一页</span>";
		   else
		       str += "<a class='btnpg nextPage' href='javascript:;'>下一页</a>";
		    $("#paging-food").append(str);
	}
</script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/userCenter.css">
<style>

.order-detail-head p{
        width:60px;
        text-align: center;
    }
    .order-detail-head h2{
        width:100px;
        text-align: center;
    }
    .order-detail-head h3{
        width:200px;
        text-align: center;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail-head h6{
         margin:0;
      padding:0;
    	float: left;
        line-height: 45px;
        font-size: 14px;
        text-align: center;
        font-weight: normal;
    	width:130px;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail-head h4{
        width: 207px;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail-head h5{
        width:80px;
        text-align: center;
        border-right: 1px solid #dcdcdc;
    }
    .order-detail{
    	height:50px;
    	width:100%;
    }
    .order-detail p{
    float:left;
    text-align: center;
        line-height: 50px;
        width: 60px;
    }
    .order-detail h2{
    line-height: 50px;
        width: 100px;
    }
    .order-detail h3{
    line-height: 50px;
        width: 200px;
    }
    .order-detail h6{
    	                float: left;
      margin:0;
      padding:0;
        text-align: center;
        line-height: 50px;
        font-size: 14px;
        font-weight: normal;
        border-right: 1px solid #dcdcdc;
        width: 130px;
    }
    .order-detail h4{
    line-height:50px;
        width: 207px;
    }
    .order-detail h5{
    line-height: 50px;
        width: 80px;
        border-right: 1px solid #dcdcdc;
          
    }
    .order-detail h5 a{
    	color:#ff6600;
    }
</style>
</head>
<body>
		<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myOrders" target="_blank">商家中心</a>
        </div>
	</div>
	<div class="container-fluid">
		<div class="col-md-2">
            <div class="title">管理中心</div>
				<ul class="left-menu">
					<li>
						<a href="adComment"><span>评论管理</span></a>
					</li>
					<li class="cur">
						<a href="adShop"><span>店铺审核</span></a>
					</li>
					<li>
	                    <a href="javascript:;"><span>修改密码</span></a>
					</li>
				</ul>
		</div>
		<div class="col-md-9">
			<div class="s-margin">
				<input id="txtSearch" type="text" placeHolder="店铺id">
				<span id="search" class="s-btn">搜索</span>
			</div>
			<input id="q" type="hidden">
			<input type="hidden" name="page" id="page" value="${page}">
			<input type="hidden" id="total" value="${total }">
			
            <div class="order">
            
            <div class="order-detail-head">
                    <p>店铺id</p>
                    <h2>店铺名</h2>
                    <p>店主名</p>
                    <h3>店主身份证号</h3>
                    <h6>联系电话</h6>
                    <h4>地址</h4>
                    <h5>状态</h5>
                    <h5>操作</h5>
                </div>
                <c:forEach items="${shops}" var="shop">
                <div class="order-detail">
                    <p><span class="id">${shop.id}</span></p>
                    <h2><a href="shop?id=${shop.id}">${shop.shopName }</a></h2>
                    <p>${shop.ownerName}</p>
                    <h3>${shop.idNumber}</h3>
                    <h6>${shop.phone}</h6>
                    <h4>${shop.address}</h4>
                    <h5>
                    	<input class="state" type="hidden" value="${shop.state}">
     					<span class="state">
						<c:if test="${shop.state == 0}">未批准</c:if>
						<c:if test="${shop.state == 1}">已批准</c:if>
						</span>
                    </h5>
                    <h5>
                    	<c:if test="${shop.state == 0}">		
						<a class="decide" href="javascript:;">批准</a>
						</c:if>
						<c:if test="${shop.state == 1}">
						<a class="decide" href="javascript:;">不批准</a>
						</c:if>
                    </h5>
                </div>
            </c:forEach>    
            </div>
			<div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                	<c:if test="${page <= 1 }">
                    <span class="btnpg disabled">上一页</span>
                    </c:if>
                    <c:if test="${page > 1 }">
                    <span class="btnpg prevPage">上一页</span>
                    </c:if>
                    <c:if test="${page == 1 }">
                    <span class="btnpg current">1</span>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <a class="btnpg" href="javascript:;">1</a>
                    </c:if>
                    <c:if test="${page - 3 > 1 }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${page - 2 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 2}</a>
                    </c:if>
                    <c:if test="${page - 1 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 1}</a>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <span class="btnpg current">${page }</span>
                    </c:if>
                    <c:if test="${page + 1 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 1}</a>
                    </c:if>
                    <c:if test="${page + 2 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 2}</a>
                    </c:if>
                    <c:if test="${page + 3 < pages }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${pages > page }">
                    <a class="btnpg" href="javascript:;">${pages}</a>
                    </c:if>
                    <c:if test="${page >= pages }">
                    <span class="btnpg disabled">下一页</span>
                    </c:if>
                    <c:if test="${page < pages }">
                    <a class="btnpg nextPage" href="javascript:;">下一页</a>
                    </c:if>
                    
                </span>
                </div>
            </div>
		</div>
	</div>
	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
		<!-- 
	<div>
		<div>
			<h1>店铺审核</h1>
			<div>
				<input id="q" type="text" placeHolder="店铺id">
				<button id="search" class="btn btn-primary active">店铺搜索</button>
			</div>
			<hr>
		</div>
		<div>
			<input type="hidden" id="page" value="${page}">
			<input type="hidden" id="total" value="${total }">
			<table class="table table-bordered">
				<tr>
					<th>店铺id</th><th>店铺名</th><th>店主名</th><th>店主身份证号</th><th>联系电话</th><th>地址</th><th>状态</th><th>操作</th>
				</tr>
			<c:forEach items="${shops}" var="shop">
				<tr>
					<td><a href="shop?id=${shop.id}"><span class="id">${shop.id}</span></a></td>
					<td>${shop.shopName}</td>
					<td>${shop.ownerName}</td>
					<td>${shop.idNumber}</td>
					<td>${shop.phone}</td>
					<td>${shop.address}</td>
					<td>
						<input class="state" type="hidden" value="${shop.state}">
						<span class="state">
						<c:if test="${shop.state == 0}">未批准</c:if>
						<c:if test="${shop.state == 1}">已批准</c:if>
						</span>
					</td>
					<td>
						<c:if test="${shop.state == 0}">		
						<button class="decide">批准</button>
						</c:if>
						<c:if test="${shop.state == 1}">
						<button class="decide">不批准</button>
						</c:if>
					</td>
				</tr>
			</c:forEach>
			</table>
			<div class="page">
				<span id="pre">上一页</span>
				<span id="next">下一页</span>
			</div>
		</div>
	</div>
	 -->
</body>
</html>