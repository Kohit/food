<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商家主页</title>
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css"></link>
<script>
function postdata(){
	var x=$("form").serializeArray();
	$("#food-list .list-wrap").empty();
	$("#food-list .list-wrap").append("正在加载...");
	$.post("index",x,function(data){
		$("#food-list .list-wrap").empty();
    	var res = eval("("+ data +")");
    	$(".paging-res").html("<span class='totalPage'>" + res.pages + "</span>页/<span class='totalRes'>" + res.total + "</span>条结果");
    	setPage(parseInt(res.page), parseInt(res.pages));
    	$.each(res.foods, function(i, food){
    		var str = "<li class='list aver";
            if (i % 5 == 0 && i != 0){
                str += " new-line";
            }
                
            str += "'><div class='img'><a href='food?id=" + food.id + "' target='_blank'>";
            str += "<img src='img/" + food.img + "' /></a></div>";
            str += "<div class='food-info'><a class='food-name' href='food?id=" + food.id + "' target='_blank'>" + food.name + "</a></div>";
            str += "<div class='food-info'>";
            str += "<p class='food-rank'><span>评价:" + food.score + "</span>&nbsp;&nbsp;&nbsp;";
            str += "<span>销量:" + food.sale + "</span></p>";
            str += "<p class='food-price'>￥" + food.price + "</p></div></li>";
           
	    $("#food-list .list-wrap").append(str);
    	});
	});  
}
$(document).ready(function(){
        $(".type .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#type").val(0);
            }
            else{
                $(this).parent().siblings().removeClass("active");
                $(this).parent().addClass("active");
                var types = new Array("综合","快餐","小炒","油炸","清蒸","汤", "甜品", "炖", "烤", "其他");
                for (i in types){
                    if (types[i] == $(this).text()){
                        $("#type").val(i);
                        break;
                    }
                }
            }
            $("#page").val(0);
            postdata();
        });
        $(".taste .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#taste").val("");
                $(this).parent().siblings().each(function(){
                    if ($(this).hasClass("active")){
                    	if ($("#taste").val() != "")
	                        $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                    	else
                    		$("#taste").val($(this).find("a").text());
                        
                    }
                });
            }else{
                $(this).parent().addClass("active");
                $("#taste").val($(this).text());
                if ($(this).text() == "综合"){
                    $(this).parent().siblings().each(function(){
                        if ($(this).hasClass("active"))
                            $(this).removeClass("active");
                    });
                }else
                    $(this).parent().siblings().each(function(){
                        if ($(this).hasClass("active")){
                            if ($(this).find("a").text() == "综合")
                                $(this).removeClass("active");
                            else
                                $("#taste").val($("#taste").val() + ";" + $(this).find("a").text());
                        }
                    });
            }
            $("#page").val(0);
            postdata();
        });
        $(".sort .filter-list a").click(function(){
            if ( $(this).parent().hasClass("active") ){
                $(this).parent().removeClass("active");
                $("#sort").val(0);
            }
            else{
                $(this).parent().siblings().removeClass("active");
                $(this).parent().addClass("active");
                var sorts = new Array("喜好度", "销量", "评价", "价格升序", "价格降序");
                for( i in sorts ){
                    if (sorts[i] == $(this).text()){
                        $("#sort").val(i);
                        break;
                    }
                }
            }
            $("#page").val(0);
            postdata();
        });
	$("#btnSearch").click(function(){
		location.href="index?search=" + encodeURIComponent($("input#txtSearch").val());
	});
	$("#btnPrice").click(function(){
		$("#page").val(0);
		postdata();
	});
});

$(document).on("click", "a.btnpg", function(){
    var cur = parseInt($("#page").val());
    var last = parseInt($(".totalPage").text());
   if ($(this).hasClass("prevPage"))
       cur--;
   else if ($(this).hasClass("nextPage"))
       cur++;
   else
       cur = parseInt($(this).text());
   $("#page").val(cur);
   setPage(cur, last);
    postdata();
});
function setPage(cur, last){
	$("#paging-food").empty();
	   var str = "";
	   if (cur <= 1)
	       str = "<span class='btnpg disabled'>上一页</span>";
	   else
	       str = "<a class='btnpg prevPage' href='javascript:;'>上一页</a>"
	   if (cur == 1)
	       str += "<span class='btnpg current'>1</span>";
	   else
	       str += "<a class='btnpg' href='javascript:;'>1</a>";
	   if (cur - 3 > 1)
	       str += "<span class='btnpg abb'>...</span>";
	   if (cur - 2 > 1)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur - 2) + "</a>";
	   if (cur - 1 > 1)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur - 1) + "</a>";
	   if (cur != 1)
	       str += "<span class='btnpg current'>" + cur + "</span>";
	   if (cur + 1 < last)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur + 1) + "</a>";
	   if (cur + 2 < last)
	       str += "<a class='btnpg' href='javascript:;'>" + (cur + 2) + "</a>";
	   if (cur + 3 < last)
	       str += "<span class='btnpg abb'>...</span>";
	   if (last > cur)
	       str += "<a class='btnpg' href='javascript:;'>" + last + "</a>";
	   if (cur >= last)
	       str += "<span class='btnpg disabled'>下一页</span>";
	   else
	       str += "<a class='btnpg nextPage' href='javascript:;'>下一页</a>";
	    $("#paging-food").append(str);
}
</script>
<style>
	    a.food-name{
	    font-size:15px;
	    font-weight:bold;
	}
	    p.food-price{
	    font-size:15px;
	    color:red;
	    }
	    div.food-info{
	    padding-top:5px;
	    }
	    	    input, button{
	    	box-sizing:inherit;
	    }
</style>
</head>
<body>
	<div class="navhead">
        <div class="nav-lf">
            <a href="index">首页</a>
            |
            <span class="shopNav">
            <a href="shop?id=${shop.id}">${shop.shopName }</a>
            </span>
        </div>
        <div class="nav-rt">
            <a href="cart" target="_blank">购物车(<span class="cartSize">${fn:length(sessionScope.cart)}</span>)</a>
        <c:choose>
			<c:when test="${!empty(sessionScope.user.id)}">
				<a href="boughtItems" target="_blank">我的订单</a>
			</c:when>
			<c:otherwise>
				<a href="login" target="_blank">登录/注册</a>
			</c:otherwise>
		</c:choose>
            <a href="myFoods" target="_blank">商家中心</a>
        </div>
	</div>
	<div class="top">
    	<div class="searchbar">
        	<div class="searchbox">
            	<span class="icon"></span>
            	<input id="txtSearch" type="text" placeholder="搜索">
	        </div>
    	    <button id="btnSearch" >搜索</button>
    	</div>
	</div>
	<form action="#" onsubmit="return false;">
        <div class="filter-block">
			筛选：
			<input id="shopId" name="id" type="hidden" value="${id}">
			<input id="search" name="search" type="hidden" value="${search}"/>
			<input id="page" name="page" type="hidden" value="${page}">
			<input id="method" name="methods" type="hidden" value="post">
			
			<input id="type" name="type" type="hidden" value="${type }">
			<input id="taste" name="taste" type="hidden" value="${taste }">
			<input id="sort" name="sort" type="hidden" value="${sort }">
            <ul class="filter-wrap type">
                <li class="filter-title">类别</li>
                <li class="filter-list">
                    <a href="javascript:;">综合</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">快餐</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">小炒</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">油炸</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">清蒸</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">汤</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">甜品</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">炖</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">烤</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">其他</a>
                </li>
            </ul>
            <ul class="filter-wrap taste">
                <li class="filter-title">口味</li>
                <li class="filter-list">
                    <a href="javascript:;">综合</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">清淡</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">辣</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">酸</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">甜</a>
                </li>

            </ul>
            <ul class="filter-wrap">
                <li class="filter-title">价格</li>
                <li class="filter-list">
                    <input class="price" name="minPrice" type="text" placeHolder="￥">
        			-
        			<input class="price" name="maxPrice" type="text" placeHolder="￥">
        			<button id="btnPrice">确定</button>
                </li>
            </ul>
            <ul class="filter-wrap sort">
                <li class="filter-title">排序</li>
                <li class="filter-list">
                    <a href="javascript:;">喜好度</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">销量</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">评价</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">价格升序</a>
                </li>
                <li class="filter-list">
                    <a href="javascript:;">价格降序</a>
                </li>
            </ul>
        </div>
        <div id="food-list">
            <ul class="list-wrap">
            	<c:forEach items="${foods}" var="item" varStatus="i">
				<c:if test="${i.count % 5 == 0}">
					<li class="list aver new-line">
				</c:if>
				<c:if test="${i.count % 5 != 0}">
					<li class="list aver">
				</c:if>
                
                    <div class="img">
                        <a href="food?id=${item.id}" target="_blank">
                            <img title="" src="img/${item.img }" />
                        </a>
                    </div>
                      <div class="food-info">
                        <a class="food-name" href="food?id=${item.id}" target="_blank">${item.name}</a>
                    </div>
                    <div>
                        <p class="food-rank">
                            <span>评价:${item.score}</span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span>销量:${item.sale}</span>
                        </p>
                        <p class="food-price">￥${item.price }</p>  
                    </div>
                </li>
            </c:forEach>
            </ul>

            </div>
            <div class="paging-wrap">
            <div class="paging">
                <span class="paging-res">共<span class="totalPage">${pages}</span>页/<span class="totalRes">${total }</span>条结果</span>
                <span id="paging-food">
                	<c:if test="${page <= 1 }">
                    <span class="btnpg disabled">上一页</span>
                    </c:if>
                    <c:if test="${page > 1 }">
                    <span class="btnpg prevPage">上一页</span>
                    </c:if>
                    <c:if test="${page == 1 }">
                    <span class="btnpg current">1</span>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <a class="btnpg" href="javascript:;">1</a>
                    </c:if>
                    <c:if test="${page - 3 > 1 }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${page - 2 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 2}</a>
                    </c:if>
                    <c:if test="${page - 1 > 1 }">
                    <a class="btnpg" href="javascript:;">${page - 1}</a>
                    </c:if>
                    <c:if test="${page != 1 }">
                    <span class="btnpg current">${page }</span>
                    </c:if>
                    <c:if test="${page + 1 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 1}</a>
                    </c:if>
                    <c:if test="${page + 2 < pages }">
                    <a class="btnpg" href="javascript:;">${page + 2}</a>
                    </c:if>
                    <c:if test="${page + 3 < pages }">
                    <span class="btnpg abb">...</span>
                    </c:if>
                    <c:if test="${pages > page }">
                    <a class="btnpg" href="javascript:;">${pages}</a>
                    </c:if>
                    <c:if test="${page >= pages }">
                    <span class="btnpg disabled">下一页</span>
                    </c:if>
                    <c:if test="${page < pages }">
                    <a class="btnpg nextPage" href="javascript:;">下一页</a>
                    </c:if>
                    
                </span>
                </div>
            </div>
    </form>
  	<div class="footer">
  		<a href="statistic" target="_blank">查看统计</a>
  		<footer>
  			<p>This website is designed and coded by Wen FengYang, Liang WeiLun and Lin NanPeng. </p>
  			<p>For all suggestions and bug reports, contact kohit[at]acm[dot]org . </p>
  		</footer>
  	</div>
</body>
</html>