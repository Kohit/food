﻿#网上订餐平台
java web 项目

---

成员 
[Kohit](https://gitlab.com/u/Kohit)
[bilicoo](https://gitlab.com/u/bilicoo)
[Lawliet007](https://gitlab.com/u/Lawliet007) 

---

git 常用指令

[] 表示要填入的内容
---
```
git clone [source].git // 拷贝位于[source]的git项目
git status // 查看项目修改状态
git add [file] [file2] [file3] [...] // 确认修改
git commit -a // 提交修改
git push // 将修改更新到repo
git pull // 从repo获取更新（与repo同步） 
```

[linux常用操作](https://github.com/Kohit/utilities/blob/master/operations%20of%20Linux.md)

---
build environment
- MySql 5.7
- Tomcat 7.0
- JDK 1.8
- Struts 2.3
- Hibernate 5.0.6

part of the UI powered by [Twitter Bootstrap](http://getbootstrap.com/). 
---

UI Reference: 
- [search.bilibili.com](http://search.bilibili.com/)
- [taobao](http://taobao.com)
- [jycinema](http://jycinema.com/user/ordersManager.aspx)

图片来自百度搜索